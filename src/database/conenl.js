import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets the conenl from the database.
 * @return {Promise} A promise with the data.
 */
function getAllCONENL() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT id_t_env_anomalie_condition_enlevement as id, CONENL, ' +
            'libelle_anomalie_conditions_enlevement as labCONENL, libelle_anomalie_conditions_SPE as labSPE ' +
            'from t_env_anomalie_condition_enlevement order by order_liste';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets a conenl by its id.
 * @param {Number} cId the id
 * @return {Promise} A promise with the data.
 */
function getCONENLById(cId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT id_t_env_anomalie_condition_enlevement as id, CONENL, ' +
            'libelle_anomalie_conditions_enlevement as labCONENL, libelle_anomalie_conditions_SPE as labSPE ' +
            'from t_env_anomalie_condition_enlevement where id = ? order by order_liste';
        const query = mysql.format(searchQuery, [cId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

/**
 * Gets the different conenl for a given call.
 * @param {Number} callId  the call Id.
 * @return {Promise} A promise with the data.
 */
function getCONENLForCall(callId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT tae.id_t_env_anomalie_condition_enlevement as id, tae.CONENL, ' +
            'libelle_anomalie_conditions_enlevement as labCONENL, libelle_anomalie_conditions_SPE as labSPE ' +
            'from t_env_anomalie_condition_enlevement tae, t_appels_anomalie_condition_enlevement tac '+
            'where tae.CONENL = tac.CONENL and tac.NumAppel = ?';
        const query = mysql.format(searchQuery, [callId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Inserts a row for each conenl related to a call.
 * @param {JSON} newData the data to insert: eg [[numAppel, CONENL], [NumAppel, CONENL]]
 * @return {Promise} A promise with the completion.
 */
function insertCONENLForCall(newData) {
    return new Promise((resolve, reject) => {
        const insertQuery = 'INSERT INTO t_appels_anomalie_condition_enlevement (NumAppel, CONENL) VALUES ?';
        const query = mysql.format(insertQuery, [newData]);
        connection.pool.query(query, (error, response) => {
            if (error) {
                reject(error);
            } else {
                resolve({ success: true });
            }
        });
    });
}


export default {
    getAllCONENL,
    getCONENLById,
    getCONENLForCall,
    insertCONENLForCall,
};
