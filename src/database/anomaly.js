import connection from './MySQLConnector';

/**
 * Gets all the common anomalies from the database.
 * @return {Promise} A promise of resolution
 */
function getAnomalies() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * from v_anomalies';
        connection.pool.query(query, (error, rows) => {
            if (error) reject(error);
            resolve(rows);
        });
    });
}

/**
 * Gets the anomalies related to the EDI.
 * @return {Promise} A promise of resolution.
 */
function getEDIAnomalies() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * from v_anomalies_EDI';
        connection.pool.query(query, (error, rows) => {
            if (error) reject(error);
            resolve(rows);
        });
    });
}

export default {
    getAnomalies,
    getEDIAnomalies,
};
