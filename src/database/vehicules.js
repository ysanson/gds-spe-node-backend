import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets all the vehicules in the database.
 * @return {Promise} A promise with the data.
 */
function getAllVehicules() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * FROM t_env_vehicules';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Getsa vehicule by its ID.
 * @param {Number} vId  the vehicule ID.
 * @return {Promise} A promise with the data.
 */
function getVehicleById(vId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * FROM t_env_vehicules WHERE NumVehicule = ?';
        const query = mysql.format(searchQuery, [vId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

export default {
    getAllVehicules,
    getVehicleById,
};
