import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Builds a statement of variable length depending on the params.
 * @param {JSON} params  The parameters.
 * @return {JSON} JSON containing both the statement and the values.
 */
function citiesBuildConditions(params) {
    const conditions = [];
    const values = [];
    if (typeof params.CP !== 'undefined') {
        conditions.push('Code_Postal = ?');
        values.push(params.CP);
    }
    if (typeof params.ville !== 'undefined') {
        conditions.push('Ville LIKE ?');
        values.push('%'+params.ville+'%');
    }
    if (typeof params.commume !== 'undefined') {
        conditions.push('Commune LIKE ?');
        values.push('%'+params.commume+'%');
    }
    if (typeof params.numZone !== 'undefined') {
        conditions.push('N°_Zone = ?');
        values.push(params.numZone);
    }
    if (typeof params.zone !== 'undefined') {
        conditions.push('zone_eq LIKE ?');
        values.push('%'+params.zone+'%');
    }
    return {
        where: conditions.length ? conditions.join(' AND ') : '1',
        values: values,
    };
}

/**
 * Gets the different cities in the database.
 * @param {JSON} params The search params.
 * @return {Promise} A promise of completion.
 */
function getCities(params) {
    return new Promise((resolve, reject) => {
        const conditions = citiesBuildConditions(params);
        const query = 'SELECT * from t_code_postaux where ' + conditions.where;
        connection.pool.query(query, conditions.values, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Retrieves a city by its ID.
 * @param {Number} cId the city id.
 * @return {Promise} A promise of resolution containing the city data.
 */
function getCityById(cId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * from t_code_postaux where Numéro = ?';
        const query = mysql.format(searchQuery, [cId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

/**
 * Creates a city with given parameters.
 * @param {JSON} newData JSON containing the new values.
 * @return {Promise} A promise of completion.
 */
function createCity(newData) {
    return new Promise((resolve, reject) => {
        const insertQuery = 'INSERT INTO t_code_postaux '+
            '(Code_Postal, Ville, Lieux_dits, Commune, N°_Zone, zone_eq) VALUES '+
            '(?, ?, ?, ?, ?, ?)';
        const query = mysql.format(insertQuery,
            [newData.CP, newData.ville, newData.lieuxDits, newData.commune, newData.numZone, newData.zone]);
        connection.pool.query(query, (error, response) =>{
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, insertId: response.insertId });
            }
        });
    });
}

/**
 * Updates a city in the database.
 * @param {JSON} newData The new data.
 * @return {Promise} A promise of completion.
 */
function updateCity(newData) {
    return new Promise((resolve, reject) => {
        const updateQuery = 'UPDATE t_code_postaux SET '+
            'Code_Postal = ?, Ville = ?, Lieux_Dits = ?, Commune = ?, N°_Zone = ?, zone_eq = ? where Numéro = ?';
        const query = mysql.format(updateQuery,
            [newData.CP, newData.ville, newData.lieuxDits, newData.commune, newData.numZone, newData.zone, newData.cId]);
        connection.pool.query(query, (error, response) => {
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, affectedRows: response.affectedRows });
            }
        });
    });
}

/**
 * Deletes a city based on its ID.
 * @param {Number} cId The city ID.
 * @return {Promise} A promise of completion.
 */
function deleteCity(cId) {
    return new Promise((resolve, reject) => {
        const deleteQuery = 'DELETE FROM t_code_postaux WHERE Numéro = ?';
        const query = mysql.format(deleteQuery, [cId]);
        connection.pool.query(query, (error, response) => {
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, affectedRows: response.affectedRows });
            }
        });
    });
}

export default {
    getCities,
    getCityById,
    createCity,
    updateCity,
    deleteCity,
};
