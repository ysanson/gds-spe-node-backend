import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets all the INFID from the database.
 * @return {Promise} A promise with all the data.
 */
function getAllINFID() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT id_t_env_INFID as id, code_INFID as code, libelle_anomalie as libelle, espece from t_env_INFID';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets INFID by its id.
 * @param {Number} iId INFID ID.
 * @return {Promise} A promise with the data.
 */
function getINFIDById(iId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT  id_t_env_INFID as id, code_INFID as code, libelle_anomalie as libelle, espece '+
            'FROM t_env_INFID where id_t_env_INFID = ?';
        const query = mysql.format(searchQuery, [iId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({error: true, notFound: true});
            } else {
                resolve(rows[0]);
            }
        });
    });
}

/**
 * Gets the INFID without the bovine ones.
 * @return {Promise} A promise with the data.
 */
function getINFIDWithoutBovine() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT id_t_env_INFID as id, code_INFID as code, libelle_anomalie as libelle, espece '+
            'from t_env_INFID where espece <> \'BOVINS\'';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

export default {
    getAllINFID,
    getINFIDById,
    getINFIDWithoutBovine,
};
