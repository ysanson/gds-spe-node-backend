import mysql from 'mysql';
import connection from './MySQLConnector';

/**
 * Looks in the database and checks if the credentials are right.
 * @param {string} username The user name.
 * @param {*} callback The callback function.
 */
function authenticateUser(username, callback) {
    const searchQuery = 'SELECT password from user where pseudo = ?';
    const query = mysql.format(searchQuery, [username]);
    let dbRes;
    connection.pool.query(query, (error, rows) => {
        if (error || rows[0] === undefined) {
            dbRes = { isPresent: false, password: '' };
        } else {
            const password = rows[0].password;
            dbRes = { isPresent: true, password: password };
        }
        callback(dbRes);
    });
}

/**
 * Gets all the users in the database.
 * @param {*} callback the function to call back.
 */
function getUsers(callback) {
    const query = 'SELECT id, firstname, lastname, pseudo, role from user';
    connection.pool.query(query, (error, rows) => {
        if (error) {
            callback({ 'error': true, 'message': 'Error occurred' + error });
        } else {
            callback(rows);
        }
    });
}

/**
 * Gets a user by its ID in the database.
 * @param {Number} userId The user ID.
 * @return {Promise} A promise of resolution with all the user values.
 */
function getUserById(userId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT id, firstname, lastname, pseudo, role from user where id = ?';
        const query = mysql.format(searchQuery, [userId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ notFound: true, message: 'The requested user was not found.' });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

/**
 * Creates a user in the database and returns its id.
 * @param {string} firstname the user firstname.
 * @param {string} lastname  the user last name.
 * @param {string} pseudo the user pseudo.
 * @param {string} password The user encrypted password.
 * @param {Number} role The user role.
 * @param {*} callback The function to call back with the user id.
 */
function createUser(firstname, lastname, pseudo, password, role, callback) {
    // eslint-disable-next-line max-len
    const insertQuery = 'INSERT INTO user (firstname, lastname, pseudo, password, role) values (?, ?, ?, ?, ?)';
    const query = mysql.format(insertQuery, [firstname, lastname, pseudo, password, role]);
    connection.pool.query(query, (error, response) => {
        let dbRes;
        if (error) {
            dbRes = { error: true, success: false, message: error };
        } else {
            dbRes = { success: true, insertId: response.insertId };
        }
        callback(dbRes);
    });
}

/**
 * Deletes a user in the database, specified by its id.
 * @param {Number} userId the user Id.
 * @return {Promise} A promise with the completion.
 */
function deleteUser(userId) {
    return new Promise((resolve, reject) => {
        const deleteQuery = 'DELETE FROM user where id = ?';
        const query = mysql.format(deleteQuery, [userId]);
        connection.pool.query(query, (error, response) => {
            if (error) reject(error);
            resolve({ success: true, affectedRows: response.affectedRows });
        });
    });
}

/**
 * Updates a user in the database. The password and salt are not touched.
 * @param {JSON} newData The new data to add.
 * @return {Promise} A promise with the completion.
 */
function updateUser(newData) {
    return new Promise((resolve, reject) => {
        const updateQuery = 'UPDATE user SET firstname = ?, lastname = ?, pseudo = ?, role = ? WHERE id = ?';
        const query = mysql.format(updateQuery,
            [newData.firstname, newData.lastname, newData.pseudo, newData.role, newData.userId]);
        connection.pool.query(query, (error, response) => {
            if (error) reject(error);
            resolve({ success: true, affectedRows: response.affectedRows });
        });
    });
}

/**
 * Gets the password from a user.
 * @param {String} username the username (pseudo)
 * @return {Promise} A promise of resolution with the password and the ID.
 */
function getPasswordFromUser(username) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT id, password from user where pseudo = ?';
        const query = mysql.format(searchQuery, [username]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ notFound: true, message: 'The requested user was not found.' });
            } else {
                resolve({ notFound: false, password: rows[0].password, id: rows[0].id });
            }
        });
    });
}

/**
 * Updates a password given an ID and the new password.
 * @param {Number} userId The user ID.
 * @param {String} newPwd The new password.
 * @return {Promise} A promise of job done.
 */
function updatePassword(userId, newPwd) {
    return new Promise((resolve, reject) => {
        const updateQuery = 'UPDATE user SET password = ? where id = ?';
        const query = mysql.format(updateQuery, [newPwd, userId]);
        connection.pool.query(query, (error, response) => {
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, affectedRows: response.affectedRows });
            }
        });
    });
}


export default {
    authenticateUser,
    getUsers,
    getUserById,
    createUser,
    deleteUser,
    updateUser,
    getPasswordFromUser,
    updatePassword,
};
