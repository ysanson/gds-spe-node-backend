import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets all the labels.
 * @return {Promise} A promise with the labels.
 */
function getAllTypdoc() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * from t_env_typdoc';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets the typdoc by id.
 * @param {*} tId the id.
 * @return {Promise} A promise with the values.
 */
function getTypdocById(tId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * from t_env_typdoc where id = ?';
        const query = mysql.format(searchQuery, [tId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

export default {
    getAllTypdoc,
    getTypdocById,
};
