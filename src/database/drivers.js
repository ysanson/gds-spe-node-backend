import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets all the drives.
 * @return {Promise} A promise of resolution.
 */
function getAllDrivers() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * FROM t_chauffeurs';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets a driver by its ID.
 * @param {Number} driverId The driver ID.
 * @return {Promise} A promise of completion.
 */
function getDriverById(driverId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * FROM t_chauffeurs WHERE `N° Conducteur` = ?';
        const query = mysql.format(searchQuery, [driverId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

export default {
    getAllDrivers,
    getDriverById,
};
