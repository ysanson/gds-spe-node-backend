import mysql from 'mysql';
import connection from './MySQLConnector';

/**
 * Retrieves all corporations from the database.
 * @return {Promise} A promise of resolution.
 */
function getCorporations() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * from t_env_filiere';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets a coporation by its id.
 * @param {Number} corpId The coporation id.
 * @return {Promise} A promise of resolution.
 */
function getCorporationById(corpId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * from t_env_filiere where reffiliere = ?';
        const query = mysql.format(searchQuery, [corpId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

/**
 * Updates a corporation with its new data.
 * @param {JSON} newData The corporation new data.
 * @return {Promise} A promise of resolution.
 */
function updateCorporation(newData) {
    return new Promise((resolve, reject) => {
        const updateQuery = 'UPDATE t_env_filiere SET nom_filiere = ?, espece = ?, adresse = ?, fax = ?';
        const query = mysql.format(updateQuery, [newData.nom_filiere, newData.espece, newData.adresse, newData.fax]);
        connection.pool.query(query, (error, response) => {
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, affectedRows: response.affectedRows });
            }
        });
    });
}

/**
 * Creates a corporation.
 * @param {JSON} corpData The new data.
 * @return {Promise} A promise of resolution.
 */
function createCorporation(corpData) {
    return new Promise((resolve, reject) => {
        const insertQuery = 'INSERT INTO t_env_filiere (nom_filiere, espece, adresse, fax) values (?, ?, ?, ?)';
        const query = mysql.format(insertQuery, [corpData.nom_filiere, corpData.espece, corpData.adresse, corpData.fax]);
        connection.pool.query(query, (error, response) =>{
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, insertId: response.insertId });
            }
        });
    });
}

/**
 * Deletes a corporation by its id.
 * @param {Number} corpId The corporation ID.
 * @return {Promise} A promise of resolution.
 */
function deleteCorporation(corpId) {
    return new Promise((resolve, reject) => {
        const deleteQuery = 'DELETE FROM t_env_filiere WHERE reffiliere = ?';
        const query = mysql.format(deleteQuery, [corpId]);
        connection.pool.query(query, (error, response) => {
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, affectedRows: response.affectedRows });
            }
        });
    });
}

export default {
    getCorporations,
    getCorporationById,
    updateCorporation,
    createCorporation,
    deleteCorporation,
};
