import mysql from 'mysql';
import connection from './MySQLConnector';

/**
 * Gets all the death causes.
 * @return {Promise} A promise with the data.
 */
function getCauses() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * FROM t_env_causes_mort_2019';
        connection.pool.query(query, (error, rows) => {
            if (error) reject(error);
            resolve(rows);
        });
    });
}

/**
 * Gets a cause by its ID.
 * @param {Number} cId The cause ID.
 * @return {Promise} A promise with the cause.
 */
function getCauseById(cId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * FROM t_env_causes_mort_2019 where id = ?';
        const query = mysql.format(searchQuery, [cId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

export default {
    getCauses,
    getCauseById,
};
