import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets all the animals from the database.
 * @return {Promise} A promise of resolution with all the animals.
 */
function getAllAnimals() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT RefCollecte, Espece, CATMAT, Race, NATSPAN, ESESPOI, TYPROD, SPE, Sexe, Nbre, Poids, POIEF, '+
        'CodePaysNumAnimal, Identification_BV, PasDIdentifiant, Indicateur1, Indicateur2, Cause_Mort, DAB, '+
        'congele, NonEnlevement, NumNonEnlevent, NoteNonEnlevement, NumAppel, DteNaiss, NumDAB, NumASDA, '+
        'RefCauseMort, CircMort from t_animaux_collectes';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets an animal by its ID.
 * @param {Number} animalID the animal ID
 * @return {Promise} A promise with the animal data.
 */
function getAnimalByID(animalID) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT RefCollecte, Espece, CATMAT, Race, NATSPAN, ESESPOI, TYPROD, SPE, Sexe, Nbre, Poids, POIEF, '+
        'CodePaysNumAnimal, Identification_BV, PasDIdentifiant, Indicateur1, Indicateur2, Cause_Mort, DAB, '+
        'congele, NonEnlevement, NumNonEnlevent, NoteNonEnlevement, NumAppel, DteNaiss, NumDAB, NumASDA, RefCauseMort, CircMort '+
        'from t_animaux_collectes where RefCollecte = ?';
        const query = mysql.format(searchQuery, [animalID]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

/**
 * Gets all the animals from a given call.
 * @param {Number} callId The call ID
 * @return {Promise} A promise of resolution with all the corresponding animals.
 */
function getAnimalsByCall(callId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT RefCollecte, ta.Espece, CATMAT, Race, NATSPAN, ESESPOI, TYPROD, SPE, Sexe, Nbre, Poids, POIEF, '+
        'CodePaysNumAnimal, Identification_BV, PasDIdentifiant, Indicateur1, Indicateur2, Cause_Mort, DAB, '+
        'congele, NonEnlevement, NumNonEnlevent, NoteNonEnlevement, NumAppel, DteNaiss, NumDAB, NumASDA, '+
        'RefCauseMort, CircMort, nat.Libelle, nat.Sigle '+
        'from t_animaux_collectes ta, t_env_natspan_siglenaturesousproduitsanimaux nat '+
        'where ta.NATSPAN = nat.Id AND NumAppel = ?';
        const query = mysql.format(searchQuery, [callId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets the animals from a period.
 * @param {Date} fromDate The from date
 * @param {Date} toDate The to date
 * @return {Promise} A promise of resolution
 */
function getAnimalsFromPeriod(fromDate, toDate) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT RefCollecte, Espece, CATMAT, Race, NATSPAN, ESESPOI, TYPROD, SPE, Sexe, Nbre, Poids, POIEF, '+
        'CodePaysNumAnimal, Identification_BV, PasDIdentifiant, Indicateur1, Indicateur2, Cause_Mort, ta.DAB, '+
        'congele, NonEnlevement, NumNonEnlevent, NoteNonEnlevement, ta.NumAppel, DteNaiss, NumDAB, NumASDA, '+
        'RefCauseMort, CircMort from t_animaux_collectes ta, t_appels tapp where ta.NumAppel = tapp.NumAppel ' +
        'and tapp.Date_Appel > ? and tapp.Date_Appel< ?';
        const query = mysql.format(searchQuery, [fromDate, toDate]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Creates an animal in the database.
 * @param {JSON} animalData The animal data.
 * @return {Promise} A promise of resolution.
 */
function createAnimal(animalData) {
    return new Promise((resolve, reject) => {
        const insertQuery = 'INSERT INTO t_animaux_collectes (Espece, CATMAT, Race, NATSPAN, ESESPOI, TYPROD, SPE, Sexe, Nbre, '+
            'Poids, POIEF, CodePaysNumAnimal, Identification_BV, PasDIdentifiant, Indicateur1, Indicateur2, Cause_Mort, DAB, '+
            'congele, NonEnlevement, NumNonEnlevent, NoteNonEnlevement, NumAppel, DteNaiss, NumDAB, NumASDA, RefCauseMort, CircMort) VALUES '+
            '(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        const query = mysql.format(insertQuery, [
            animalData.Espece, animalData.CATMAT, animalData.Race, animalData.NATSPAN, animalData.ESESPOI, animalData.TYPROD,
            animalData.SPE, animalData.Sexe, animalData.Nbre, animalData.Poids, animalData.POIEF,
            animalData.CodePaysNumAnimal, animalData.Identification_BV,
            animalData.PasDIdentifiant, animalData.Indicateur1, animalData.Indicateur2, animalData.Cause_Mort, animalData.DAB,
            animalData.congele, animalData.NonEnlevement, animalData.NumNonEnlevent,
            animalData.NoteNonEnlevement, animalData.NumAppel, animalData.DteNaiss, animalData.NumDAB, animalData.NumASDA,
            animalData.RefCauseMort, animalData.CircMort]);
        connection.pool.query(query, (error, response) =>{
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, insertId: response.insertId });
            }
        });
    });
}

/**
 * Updatres n animal in the database.
 * @param {JSON} animalData The new data.
 * @return {Promise} A promise of resolution.
 */
function updateAnimal(animalData) {
    return new Promise((resolve, reject) => {
        const updateQuery = 'UPDATE t_animaux_collectes SET Espece = ?, CATMAT = ?, Race = ?, NATSPAN = ?, ESESPOI = ?, '+
            'TYPROD = ?, SPE = ?, Sexe = ?, Nbre = ?, Poids = ?, POIEF = ?, CodePaysNumAnimal = ?, Identification_BV = ?, '+
            'PasDIdentifiant = ?, Indicateur1 = ?, Indicateur2 = ?, Cause_Mort = ?, DAB = ?, congele = ?, NonEnlevement = ?, '+
            'NumNonEnlevent = ?, NoteNonEnlevement = ?, NumAppel = ?, DteNaiss = ?, NumDAB = ?, NumASDA = ?, '+
            'RefCauseMort = ?, CircMort = ? WHERE RefCollecte = ?';
        const query = mysql.format(updateQuery, [
            animalData.Espece, animalData.CATMAT, animalData.Race, animalData.NATSPAN, animalData.ESESPOI, animalData.TYPROD,
            animalData.SPE, animalData.Sexe, animalData.Nbre, animalData.Poids, animalData.POIEF,
            animalData.CodePaysNumAnimal, animalData.Identification_BV,
            animalData.PasDIdentifiant, animalData.Indicateur1, animalData.Indicateur2, animalData.Cause_Mort, animalData.DAB,
            animalData.congele, animalData.NonEnlevement, animalData.NumNonEnlevent,
            animalData.NoteNonEnlevement, animalData.NumAppel, animalData.DteNaiss, animalData.NumDAB, animalData.NumASDA,
            animalData.RefCauseMort, animalData.CircMort, animalData.RefCollecte]);
        connection.pool.query(query, (error, response) => {
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, affectedRows: response.affectedRows });
            }
        });
    });
}

/**
 * Gets the data from the cattle that has been removed.
 * Searches for the cattle in the period, then adds fields from the call related and the farm.
 * @param {Date} fromDate The date to search from
 * @param {Date} toDate The date to seach to
 * @return {Promise} A promise with the data inside.
 */
function getCattleRemovalsData(fromDate, toDate) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT RefCollecte, ta.Espece, CATMAT, Race, NATSPAN, ESESPOI, TYPROD, SPE, Sexe, Nbre, Poids, POIEF, '+
        'CodePaysNumAnimal, Identification_BV, PasDIdentifiant, Indicateur1, Indicateur2, Cause_Mort, ta.DAB, '+
        'congele, NonEnlevement, NumNonEnlevent, NoteNonEnlevement, ta.NumAppel, DteNaiss, NumDAB, NumASDA, '+
        'RefCauseMort, CircMort, '+
        'Nom_Det, Prenom_Det, EDEV2, SIRETV2, '+
        'Telephone1, Portable1, Portable2, num_voie_dom, nom_voie_dom, Adrs_Dom, CP_Dom, Commune_Dom, '+
        'Date_Appel, Heure_Appel, Date_Enlev, Heure_Enlev, tapp.Num_Certificat, ' +
        'nat.Libelle ' +
        'FROM t_animaux_collectes ta, t_appels tapp, t_etablissement te, '+
        't_env_natspan_siglenaturesousproduitsanimaux nat '+
        'WHERE ta.NumAppel = tapp.NumAppel AND (ta.Espece = \'BOVINS\' OR ta.Espece = \'VEAUX\') ' +
        'AND tapp.Id_Etablissement = te.Id_Etablissement ' +
        'AND ta.NATSPAN = nat.Id '+
        'AND tapp.Date_Enlev > ? AND tapp.Date_Enlev < ? AND tapp.Enlevement = -1 ';
        const query = mysql.format(searchQuery, [fromDate, toDate]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

export default {
    getAllAnimals,
    getAnimalByID,
    getAnimalsByCall,
    getAnimalsFromPeriod,
    createAnimal,
    updateAnimal,
    getCattleRemovalsData,
};
