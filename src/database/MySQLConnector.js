import 'dotenv/config';
import { createPool } from 'mysql';

const pool = createPool({
    connectionLimit: 100,
    host: process.env.DBADDR,
    user: process.env.DBUSER,
    password: process.env.DBPWD,
    database: 'spe_new',
    debug: false,
});

export default {
    pool,
};
