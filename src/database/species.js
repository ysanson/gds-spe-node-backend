import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets all the labels.
 * @return {Promise} A promise with the labels.
 */
function getAllSpecies() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * from t_env_espece';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets the moraspe by id.
 * @param {Number} sId the id.
 * @return {Promise} A promise with the values.
 */
function getSpeciesById(sId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * from t_env_moraspe where RefEspece = ?';
        const query = mysql.format(searchQuery, [sId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

export default {
    getAllSpecies,
    getSpeciesById,
};
