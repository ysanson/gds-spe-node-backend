import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets all the calls from the database.
 * It reaches to the v-appels view.
 * @return {Promise} A promise containing the calls.
 */
function getAllCalls() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * '+
        'FROM v_liste_appels WHERE `Date APPEL` > curdate() - interval 6 month ' +
        'ORDER BY `REF APPEL` DESC';
        connection.pool.query(query, (error, rows) =>{
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets a call by its ID.
 * @param {Number} callId The call ID.
 * @return {Promise} A promise with the call data.
 */
function getCallById(callId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT NumAppel, PASSSAP, TYPDOC, Date_Appel, Heure_Appel, TTYGIS, ta.Id_Etablissement, autorisation_veto, DAB, '+
        'Remarques, Num_Certificat, Date_Enlev, Heure_Enlev, Enlevement, appel_tx_chauffeur, annulation, appel_resir, dte_autori_veto, '+
        'dte_appel_trans, appel_noneffec, date_noneffec, date_cocher_enlev, centre_trait_bon, appel_auto, autopsie, RefBonCtrait, MORASPE, '+
        'Nom_Det, Prenom_Det, EDEV2, SIRETV2 '+
        'FROM t_appels ta, t_etablissement te '+
        'WHERE NumAppel = ? AND ta.Id_Etablissement = te.Id_Etablissement';
        const query = mysql.format(searchQuery, [callId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

/**
 * Gets the calls of a given year.
 * @param {Number} callYear The call year.
 * @return {Promise} A promise containing all the calls from a year.
 */
function getCallsByYear(callYear) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * FROM v_liste_appels '+
            'WHERE `DATE APPEL` BETWEEN ? AND ? ' +
            'ORDER BY `REF APPEL` DESC';
        const query = mysql.format(searchQuery, [callYear+'-01-01', callYear+'-12-31']);
        connection.pool.query(query, (error, rows) =>{
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets the calls from the day before to today.
 * @return {Promise} A promise with all the calls.
 */
function getCallsFromYesterday() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * '+
        'FROM v_liste_appels WHERE `DATE APPEL` > curdate() - interval 2 day ' +
        'ORDER BY `REF APPEL` DESC';
        connection.pool.query(query, (error, rows) =>{
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets the calls from the day before to today.
 * @return {Promise} A promise with all the calls.
 */
function getCallsFromTwoDays() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * '+
        'FROM v_liste_appels WHERE `DATE APPEL` > curdate() - interval 3 day ' +
        'ORDER BY `REF APPEL` DESC';
        connection.pool.query(query, (error, rows) =>{
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets all the years at when there was a call.
 * @return {Promise} A promise of resolution.
 */
function getCallYears() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT DISTINCT YEAR(Date_Appel) as year FROM t_appels';
        connection.pool.query(query, (error, rows) =>{
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}
/**
 * Creates a call in the database.
 * @param {JSON} callData The call data.
 * @return {Promise} A promise of resolution.
 */
function createCall(callData) {
    return new Promise((resolve, reject) => {
        const insertQuery = 'INSERT INTO t_appels (PASSSAP, TYPDOC, Date_Appel, Heure_Appel, TTYGIS, Id_Etablissement, autorisation_veto, DAB, '+
        'Remarques, Num_Certificat, Date_Enlev, Heure_Enlev, Enlevement, appel_tx_chauffeur, annulation, appel_resir, dte_autori_veto, '+
        'dte_appel_trans, appel_noneffec, date_noneffec, date_cocher_enlev, centre_trait_bon, appel_auto, autopsie, '+
        'RefBonCtrait, MORASPE) '+
        'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        try {
            const query = mysql.format(insertQuery, [
                callData.PASSSAP, callData.TYPDOC, callData.Date_Appel, callData.Heure_Appel,
                callData.TTYGIS, callData.Id_Etablissement, callData.autorisation_veto,
                callData.DAB, callData.Remarques, callData.Num_Certificat, callData.Date_Enlev,
                callData.Heure_Enlev, callData.Enlevement, callData.appel_tx_chauffeur, callData.annulation,
                callData.appel_resir, callData.dte_autori_veto, callData.dte_appel_trans, callData.appel_noneffec, callData.date_noneffec,
                callData.date_cocher_enlev, callData.centre_trait_bon, callData.appel_auto,
                callData.autopsie, callData.RefBonCtrait, callData.MORASPE]);
            connection.pool.query(query, (error, response) =>{
                if (error) {
                    reject(error);
                } else {
                    resolve({ success: true, insertId: response.insertId });
                }
            });
        } catch (error) {
            reject(error);
        }
    });
}

/**
 * Updates a call in the database.
 * @param {JSON} callData The new data
 * @return {Promise} A promise of resolution.
 */
function updateCall(callData) {
    return new Promise((resolve, reject) => {
        const updateQuery = 'UPDATE t_appels SET PASSSAP = ?, TYPDOC = ?, Date_Appel = ?, Heure_Appel = ?, '+
            'TTYGIS = ?, Id_Etablissement = ?, autorisation_veto = ?, DAB = ?, '+
            'Remarques = ?, Num_Certificat = ?, Date_Enlev = ?, Heure_Enlev = ?, Enlevement = ?, ' +
            'appel_tx_chauffeur = ?, annulation = ?, appel_resir = ?, dte_autori_veto = ?, '+
            'dte_appel_trans = ?, appel_noneffec = ?, date_noneffec = ?, date_cocher_enlev = ?,'+
            'centre_trait_bon = ?, appel_auto = ?, autopsie = ?, RefBonCtrait = ?, MORASPE = ? '+
            'WHERE NumAppel = ?';
        const query = mysql.format(updateQuery, [
            callData.PASSSAP, callData.TYPDOC, callData.Date_Appel, callData.Heure_Appel,
            callData.TTYGIS, callData.Id_Etablissement, callData.autorisation_veto,
            callData.DAB, callData.Remarques, callData.Num_Certificat, callData.Date_Enlev,
            callData.Heure_Enlev, callData.Enlevement, callData.appel_tx_chauffeur,
            callData.annulation, callData.appel_resir, callData.dte_autori_veto,
            callData.dte_appel_trans, callData.appel_noneffec, callData.date_noneffec,
            callData.date_cocher_enlev, callData.centre_trait_bon, callData.appel_auto,
            callData.autopsie, callData.RefBonCtrait, callData.MORASPE, callData.NumAppel]);
        connection.pool.query(query, (error, response) => {
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, affectedRows: response.affectedRows });
            }
        });
    });
}

/**
 * Gets the calls taken for a specific period.
 * @param {Date} fromDate The start date
 * @param {Date} toDate The end date
 * @return {Promise} A promise with the data.
 */
function getRemovalsFromPeriod(fromDate, toDate) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT NumAppel, PASSSAP, TYPDOC, Date_Appel, Heure_Appel, TTYGIS, ta.Id_Etablissement, autorisation_veto, DAB, '+
            'Remarques, Num_Certificat, Date_Enlev, Heure_Enlev, Enlevement, appel_tx_chauffeur, annulation, appel_resir, dte_autori_veto, '+
            'dte_appel_trans, appel_noneffec, date_noneffec, date_cocher_enlev, centre_trait_bon, appel_auto, autopsie, RefBonCtrait, MORASPE, '+
            'Nom_Det, Prenom_Det, EDEV2, SIRETV2 FROM t_appels ta, t_etablissement te WHERE ta.Date_Enlev > ? and ta.Date_Enlev < ? '+
            'AND ta.Id_Etablissement = te.Id_Etablissement AND Enlevement = -1';
        const query = mysql.format(searchQuery, [fromDate, toDate]);
        connection.pool.query(query, (error, rows) =>{
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

export default {
    getAllCalls,
    getCallById,
    getCallsByYear,
    getCallsFromYesterday,
    getCallsFromTwoDays,
    getCallYears,
    createCall,
    updateCall,
    getRemovalsFromPeriod,
};
