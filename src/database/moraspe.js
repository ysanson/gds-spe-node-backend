import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets all the labels.
 * @return {Promise} A promise with the labels.
 */
function getAllMoraspe() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT Code, MotifDeRattachementSPE as label from t_env_moraspe';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets the moraspe by id.
 * @param {*} mId the id.
 * @return {Promise} A promise with the values.
 */
function getMoraspeById(mId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT Code, MotifDeRattachementSPE as label from t_env_moraspe where Code = ?';
        const query = mysql.format(searchQuery, [mId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

export default {
    getAllMoraspe,
    getMoraspeById,
};
