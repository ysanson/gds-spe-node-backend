import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets all the centers from the database.
 * @return {Promise} A promise of resolution.
 */
function getAllCenters() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * from t_centre_traitement';
        connection.pool.query(query, (error, rows) => {
            if (error) reject(error);
            resolve(rows);
        });
    });
}

/**
 * Gets a center by its ID.
 * @param {Number} cId The center ID.
 * @return {Promise} A promise of resolution.
 */
function getCenterById(cId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * from t_centre_traitement where num_centre_trait = ?';
        const query = mysql.format(searchQuery, [cId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

export default {
    getAllCenters,
    getCenterById,
};
