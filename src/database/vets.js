import mysql from 'mysql';
import connection from './MySQLConnector';

/**
 * Gets all the vets from the database.
 * @return {Promise} A promise of resolution
 */
function getAllVets() {
    return new Promise((resolve, reject) => {
        const query = 'select * from t_veto';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets a vet from the database by its id.
 * @param {Number} vId The vet Id.
 * @return {Promise} A promise of resolution.
 */
function getVetById(vId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * from t_veto where Ref_veto = ?';
        const query = mysql.format(searchQuery, [vId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

/**
 * Deletes a vet by its ID.
 * @param {Number} vId The vet Id.
 * @return {Promise} A promise of resolution.
 */
function deleteVet(vId) {
    return new Promise((resolve, reject) => {
        const deleteQuery = 'DELETE FROM t_veto WHERE Ref_Veto = ?';
        const query = mysql.format(deleteQuery, [vId]);
        connection.pool.query(query, (error, response) => {
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, affectedRows: response.affectedRows });
            }
        });
    });
}

/**
 * Creates a nex vet in the database.
 * @param {JSON} vetData The vet data.
 * @return {Promise} A promise of resolution.
 */
function createVet(vetData) {
    return new Promise((resolve, reject) => {
        const insertQuery = 'INSERT INTO t_veto (`Noms-Prenoms`, Cabinet, Adresse, `Code Postal`, `Lieux-dits`, Ville, Telephone, Fax)'+
        ' VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
        const query = mysql.format(insertQuery,
            [vetData['Noms-Prenoms'], vetData.Cabinet, vetData.Adresse, vetData['Code Postal'],
                vetData['Lieux-dits'], vetData.Ville, vetData.Telephone, vetData.Fax]);
        connection.pool.query(query, (error, response) =>{
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, insertId: response.insertId });
            }
        });
    });
}

/**
 * Updates a vet inthe database.
 * @param {JSON} vetData  The vet's new data.
 * @return {Promise} A promise of resolution.
 */
function updateVet(vetData) {
    return new Promise((resolve, reject) => {
        const updateQuery = 'UPDATE t_veto SET `Noms-Prenoms` = ?, Cabinet = ?, ' +
            'Adresse = ?, `Code Postal` = ?, `Lieux-dits` = ?, Ville = ?, Telephone = ?, Fax = ? where Ref_Veto = ?';
        const query = mysql.format(updateQuery,
            [vetData['Noms-Prenoms'], vetData.Cabinet, vetData.Adresse, vetData['Code Postal'],
                vetData['Lieux-dits'], vetData.Ville, vetData.Telephone, vetData.Fax, vetData.vId]);
        connection.pool.query(query, (error, response) => {
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, affectedRows: response.affectedRows });
            }
        });
    });
}

export default {
    getAllVets,
    getVetById,
    deleteVet,
    createVet,
    updateVet,
};
