import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets the prioritary removals along with their expiry dates.
 * @return {Promise} A promise with the data.
 */
function getPrioritaryRemovals() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT Z, NumAppel, `APPEL HORODATE` as datetime, date_PEC, debut_delai, delai_enlevement, '+
            'depassement_enlevement, ELEVEUR, Espece, Nbre, Poids, Remarques, autorisation_veto, Adresse, Ville '+
            ' FROM v_planification_tournee WHERE delai_enlevement = 0 OR depassement_enlevement > 0';
        connection.pool.query(query, (error, rows) => {
            if (error) reject(error);
            resolve(rows);
        });
    });
}

/**
 * Gets the removals for the day by zone.
 * @param {String} zone The zone (OUEST, MILIEU, SALAZIE, EST, SUD, TOUS)
 * @return {Promise} A promise of resolution.
 */
function getRemovalsByZone(zone) {
    return new Promise((resolve, reject) => {
        let searchQuery = 'SELECT NumAppel, EDEV2, SIRETV2, NAPI, `APPEL HORODATE` as datetime, '+
            'ELEVEUR, Espece, Nbre, Poids, Lot, Remarques, autorisation_veto, Adresse, Ville, '+
            'Telephone1, Portable1, Portable2, Z, Commune, appel_auto, Id_Atelier, delai_enlevement, depassement_enlevement '+
            'FROM v_planification_tournee WHERE Z like ?';
        if (zone === 'OUEST') {
            searchQuery += ' OR Z = \'MILIEU\'';
        }
        const query = mysql.format(searchQuery, [zone]);
        connection.pool.query(query, (error, rows) => {
            if (error) reject(error);
            resolve(rows);
        });
    });
}

export default {
    getPrioritaryRemovals,
    getRemovalsByZone,
};
