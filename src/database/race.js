import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets all the races in the database.
 * @return {Promise} A promise with the data.
 */
function getAllRaces() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * from t_env_race';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets a race by its id.
 * @param {Number} rId The race Id.
 * @return {Promise} A Promise with the data.
 */
function getRaceById(rId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * from t_env_race where `N°`= ?';
        const query = mysql.format(searchQuery, [rId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

/**
 * Gets the cow races from the database.
 * @return {Promise} A promise with the data.
 */
function getCowRaces() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * from t_env_race where Espece = \'BOVINS\'';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

export default {
    getAllRaces,
    getRaceById,
    getCowRaces,
};
