import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets all the ttygis from the database.
 * @return {Promise} A promise with the data.
 */
function getAllTTYGIS() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT Id, CodeDesGisements as sigle, Libelle_des_gisements as libelle, Commentaires from t_env_typedegisement_ttygis';
        connection.pool.query(query, (error, rows) =>{
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets a ttygis bby its id.
 * @param {Number} tId The id.
 * @return {Promise} A promise with the data.
 */
function getTTYGISById(tId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT Id, CodeDesGisements as sigle, Libelle_des_gisements as libelle, Commentaires '+
            'from t_env_typedegisement_ttygis where Id = ?';
        const query = mysql.format(searchQuery, [tId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

export default {
    getAllTTYGIS,
    getTTYGISById,
};
