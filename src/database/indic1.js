import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets all the INFID from the database.
 * @return {Promise} A promise with all the data.
 */
function getAllIndic1() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * from t_animaux_collectes_indicateur1';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets INFID by its id.
 * @param {Number} iId INFID ID.
 * @return {Promise} A promise with the data.
 */
function getIndic1ById(iId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * FROM t_animaux_collectes_indicateur1 where Id = ?';
        const query = mysql.format(searchQuery, [iId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

export default {
    getAllIndic1,
    getIndic1ById,
};
