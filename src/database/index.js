import pool from './MySQLConnector';
import user from './user';
import installation from './installation';
import city from './city';
import corporation from './corporation';
import vet from './vets';
import call from './calls';
import moraspe from './moraspe';
import typdoc from './typdoc';
import animal from './animals';
import natspan from './natspan';
import infid from './infid';
import indic1 from './indic1';
import indic2 from './indic2';
import race from './race';
import tour from './tour';
import vehicule from './vehicules';
import driver from './drivers';
import species from './species';
import ttygis from './ttygis';
import conenl from './conenl';
import planning from './planning';
import anomaly from './anomaly';
import center from './center';
import deathCauses from './deathCauses';

export default {
    pool,
    user,
    installation,
    city,
    corporation,
    vet,
    call,
    moraspe,
    typdoc,
    animal,
    natspan,
    infid,
    indic1,
    indic2,
    race,
    tour,
    vehicule,
    driver,
    species,
    ttygis,
    conenl,
    planning,
    anomaly,
    center,
    deathCauses,
};
