import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets all the natspan from the database.
 * @return {Promise} A promise containing the data.
 */
function getAllNatspan() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * from t_env_natspan_siglenaturesousproduitsanimaux where actif_base_SPE = 1 order by ordre_liste';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets a natspan by its id.
 * @param {Number} nId  the Natspan ID.
 * @return {Promise} A promise with the data.
 */
function getNatspanById(nId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * FROM t_env_natspan_siglenaturesousproduitsanimaux where Id = ?';
        const query = mysql.format(searchQuery, [nId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

export default {
    getAllNatspan,
    getNatspanById,
};
