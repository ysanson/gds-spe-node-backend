import mysql from 'mysql';
import connection from './MySQLConnector';

/**
 * Builds a statement of variable length depending on the params.
 * @param {JSON} params The variable parameters.
 * @return {JSON} Contains the where clauses and the values.
 */
function installationBuildConditions(params) {
    const conditions = [];
    const values = [];
    if (typeof params.ede !== 'undefined') {
        conditions.push('EDEV2 = ?');
        values.push(params.ede);
    }
    if (typeof params.siret !== 'undefined') {
        conditions.push('SIRETV2 = ?');
        values.push(params.siret);
    }
    if (typeof params.poula !== 'undefined') {
        conditions.push('POULA = ?');
        values.push(params.poula);
    }
    if (typeof params.tva !== 'undefined') {
        conditions.push('TVA = ?');
        values.push(params.tva);
    }
    if (typeof params.dosequ !== 'undefined') {
        conditions.push('DOSEQU = ?');
        values.push(params.dosequ);
    }
    if (typeof params.name !== 'undefined') {
        conditions.push('Nom_Det LIKE ?');
        values.push('%'+params.name+'%');
    }
    if (typeof params.zoneDom !== 'undefined') {
        conditions.push('Secteur_dom LIKE ?');
        values.push('%'+params.zoneDom+'%');
    }
    if (typeof params.zoneExp !== 'undefined') {
        conditions.push('Secteur_exploit LIKE ?');
        values.push('%'+params.zoneExp+'%');
    }
    if (typeof params.PCDom !== 'undefined') {
        conditions.push('CP_Dom = ?');
        values.push(params.PCDom);
    }
    if (typeof params.PCExp !== 'undefined') {
        conditions.push('CP_Exploit = ?');
        values.push(params.PCExp);
    }
    if (typeof params.active !== 'undefined') {
        conditions.push('Actif = ?');
        if (params.active === 'true') {
            values.push(1);
        } else {
            values.push(0);
        }
    }
    return {
        where: conditions.length ? conditions.join(' AND ') : '1',
        values: values,
    };
}

/**
 * Gets all the current installations in the database.
 * @param {JSON} params The object filters.
 * @return {Promise} A promise containing all the installations.
 */
function getAllInstallations(params) {
    return new Promise((resolve, reject) => {
        const conditions = installationBuildConditions(params);
        let query = 'SELECT * from t_etablissement et, t_env_filiere fi WHERE ' + conditions.where +
            ' AND et.filiere = fi.reffiliere';
        connection.pool.query(query, conditions.values, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                const response = rows;
                query = 'select * from t_etablissement WHERE (filiere IS NULL OR filiere NOT IN (select reffiliere from t_env_filiere)) '+
                'AND ' + conditions.where;
                connection.pool.query(query, conditions.values, (error, rows) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(response.concat(rows));
                    }
                });
            }
        });
    });
}

/**
 * Retrieves an installation by its ID.
 * @param {Number} insId the installation id.
 * @return {Promise} A promise of resolution containing the installation data.
 */
function getInstallationByID(insId) {
    return new Promise((resolve, reject) => {
        let searchQuery = 'SELECT * from t_etablissement et, t_env_filiere fi where et.Id_Etablissement = ? ' +
            'AND et.filiere = fi.reffiliere';
        let query = mysql.format(searchQuery, [insId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                searchQuery = 'SELECT * from t_etablissement et where et.Id_Etablissement = ?';
                query = mysql.format(searchQuery, [insId]);
                connection.pool.query(query, (error, rows) => {
                    if (error) {
                        reject(error);
                    } else if (rows[0] === undefined) {
                        resolve({ error: true, notFound: true });
                    } else {
                        resolve(rows[0]);
                    }
                });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

/**
 * Gets all the animals collected from an installation.
 * It caters to a period of time.
 * @param {Date} fromDate The date to collect from
 * @param {Date} toDate The date to collect to
 * @param {Number} insId The installation ID.
 * @return {Promise} A promise with the data.
 */
function getInstallationAnimals(fromDate, toDate, insId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT ap.NumAppel, ap.Date_Appel, ap.Heure_Appel, ap.Date_Enlev, ap.Heure_Enlev, ap.Num_Certificat, '+
            'an.Espece, an.Sexe, an.Nbre, an.Poids, an.Cause_Mort, CONCAT(an.CodePaysNumAnimal, an.Identification_BV) as Identification, ' +
            'an.NumDAB, an.NumASDA from t_appels ap, t_etablissement et, t_animaux_collectes an '+
            'where et.Id_Etablissement = ? AND et.Id_Etablissement = ap.Id_Etablissement AND an.NumAppel = ap.NumAppel '+
            'AND ap.Date_Appel > ? AND ap.Date_Appel < ?';
        const query = mysql.format(searchQuery, [insId, fromDate, toDate]);
        connection.pool.query(query, (error, rows) => {
            if (error) reject(error);
            resolve(rows);
        });
    });
}

/**
 * Creates an installation in the database.
 * @param {JSON} insData JSON containing all the values.
 * @return {Promise} A promise of resolution.
 */
function createInstallation(insData) {
    return new Promise((resolve, reject) => {
        const insertQuery = 'INSERT INTO t_etablissement '+
            '(EDEV2, SIRETV2, NPAC, POULA, TVA, DOSEQU, NAPI, Type_Atelier, Nom_Det, Prenom_Det, '+
            'Adrs_Dom, num_voie_dom, nom_voie_dom, extension_voie_dom, Lieu_dit_Dom, Ville_Dom, CP_Dom, Commune_Dom, '+
            'Secteur_Dom, Zone_Dom, Telephone1, Portable1, Portable2, '+
            'nom_veto, fratrie, filiere, Actif) ' +
            'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        const query = mysql.format(insertQuery,
            [insData.ede, insData.siret, insData.npac, insData.poula, insData.tva, insData.dosequ, insData.napi,
                insData.typeAtelier, insData.detName, insData.detFirstname,
                insData.domAddr, insData.domNum, insData.domName, insData.domExt, insData.lieuDitDom, insData.cityDom, insData.postalCodeDom,
                insData.commDom, insData.sectorDom, insData.zoneDom, insData.tel1, insData.tel2, insData.tel3,
                insData.vet, insData.frat, insData.filiere, insData.actif]);
        connection.pool.query(query, (error, response) =>{
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, insertId: response.insertId });
            }
        });
    });
}

/**
 * Deletes an installation in the database.
 * @param {Number} insId The installation ID.
 * @return {Promise} A promise of completion.
 */
function deleteInstallation(insId) {
    return new Promise((resolve, reject) => {
        const deleteQuery = 'DELETE FROM t_etablissement WHERE Id_Etablissement = ?';
        const query = mysql.format(deleteQuery, [insId]);
        connection.pool.query(query, (error, response) => {
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, affectedRows: response.affectedRows });
            }
        });
    });
}

/**
 * Updates an installation with its new parameters.
 * @param {JSON} newData The new data.
 * @return {Promise} A promise of completion.
 */
function updateInstallation(newData) {
    return new Promise((resolve, reject) => {
        const updateQuery = 'UPDATE t_etablissement SET ' +
            'EDEV2 = ?, SIRETV2 = ?, NPAC = ?, POULA = ?, TVA = ?, DOSEQU = ?, NAPI = ?, Type_Atelier = ?, '+
            'Nom_Det = ?, Prenom_Det = ?, '+
            'Adrs_Dom = ?, num_voie_dom = ?, nom_voie_dom = ?, extension_voie_dom = ?, Lieu_dit_Dom = ?, '+
            'Ville_Dom = ?, CP_Dom = ?, Commune_Dom = ?, Secteur_Dom = ?, Zone_Dom = ?, Telephone1 = ?, Portable1 = ?, Portable2 = ?, '+
            'nom_veto = ?, fratrie = ?, filiere = ?, Actif = ? WHERE Id_Etablissement = ?';
        const query = mysql.format(updateQuery,
            [newData.ede, newData.siret, newData.npac, newData.poula, newData.tva, newData.dosequ, newData.napi,
                newData.typeAtelier, newData.detName, newData.detFirstname,
                newData.domAddr, newData.domNum, newData.domName, newData.domExt, newData.lieuDitDom, newData.cityDom, newData.postalCodeDom,
                newData.commDom, newData.sectorDom, newData.zoneDom, newData.tel1, newData.tel2, newData.tel3,
                newData.vet, newData.frat, newData.filiere, newData.actif, newData.insId]);
        connection.pool.query(query, (error, response) => {
            if (error) {
                reject(error);
            } else {
                resolve({ success: true, affectedRows: response.affectedRows });
            }
        });
    });
}

export default {
    getAllInstallations,
    getInstallationByID,
    getInstallationAnimals,
    createInstallation,
    deleteInstallation,
    updateInstallation,
};
