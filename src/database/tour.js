import connection from './MySQLConnector';
import mysql from 'mysql';

/**
 * Gets all the tours from the database.
 * Limited to one year.
 * @return {Promise} A promise with the data.
 */
function getAllTours() {
    return new Promise((resolve, reject) => {
        const query = 'SELECT * FROM t_bon_onyx tb, t_env_vehicules tv, t_centre_traitement tc '+
            'where tb.vehicule = tv.NumVehicule and tb.centre_traitmt = tc.num_centre_trait and tb.date_reception  > curdate() - interval 1 year '+
            'order by refbonOnyx DESC';
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets the tour by ID.
 * @param {Number} tourId The tour ID
 * @return {Promise} A promise with the data.
 */
function getTourById(tourId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * FROM t_bon_onyx tb, t_env_vehicules tv, t_centre_traitement tc '+
        'where tb.vehicule = tv.NumVehicule and tb.centre_traitmt = tc.num_centre_trait and RefbonOnyx = ?';
        const query = mysql.format(searchQuery, [tourId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else if (rows[0] === undefined) {
                resolve({ error: true, notFound: true });
            } else {
                resolve(rows[0]);
            }
        });
    });
}

/**
 * Gets all the tours from a certain date.
 * @param {String} tourDate The date in String format, like 'AAAA-MM-DD'.
 * @return {Promise} A promise with the data.
 */
function getToursByDate(tourDate) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT * FROM t_bon_onyx WHERE DATE(date_reception) = ?';
        const query = mysql.format(searchQuery, [tourDate]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Gets the calls associated with a tour in the database.
 * @param {Number} tourId The tour ID
 * @return {Promise} A promise with the data.
 */
function getCallsForTour(tourId) {
    return new Promise((resolve, reject) => {
        const searchQuery = 'SELECT NumAppel, PASSSAP, TYPDOC, Date_Appel, Heure_Appel, TTYGIS, ta.Id_Etablissement, autorisation_veto, DAB, '+
        'Remarques, Num_Certificat, Date_Enlev, Heure_Enlev, Enlevement, appel_tx_chauffeur, annulation, appel_resir, dte_autori_veto, '+
        'dte_appel_trans, appel_noneffec, date_noneffec, date_cocher_enlev, centre_trait_bon, appel_auto, autopsie, RefBonCtrait, MORASPE, '+
        'Nom_Det, Prenom_Det, EDEV2, SIRETV2 '+
        'FROM t_appels ta, t_etablissement te WHERE RefBonCtrait = ? AND ta.Id_Etablissement = te.Id_Etablissement';
        const query = mysql.format(searchQuery, [tourId]);
        connection.pool.query(query, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

/**
 * Creates a tour in the database.
 * @param {JSON} tourData The new data.
 * @return {Promise} A promise of resolution
 */
function createTour(tourData) {
    return new Promise((resolve, reject) => {
        const insertQuery = 'INSERT INTO t_bon_onyx (TIDTOUR, num_bon, vehicule, Chauffeur_1, Chauffeur_2, Heure_Deb_Tour, '+
            'Heure_Fin_Tour, Kilometre_Deb_Tour, Kilometre_Fin_Tour, Poids_Charge_Vehicule, Poids_Vide_Vehicule, poidsNet, '+
            'Poids_ApresSousProduitsCat2, date_reception, heure_reception, Incident_De_Pesee, Date_enr, centre_traitmt, ' +
            'chauffeur1_horaire_debut, chauffeur1_horaire_fin, chauffeur2_horaire_début, chauffeur2_horaire_fin) '+
            'VALUES ' +
            '(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        const query = mysql.format(insertQuery, [
            tourData.TIDTOUR, tourData.num_bon, tourData.vehicule, tourData.Chauffeur_1, tourData.Chauffeur_2, tourData.Heure_Deb_Tour,
            tourData.Heure_Fin_Tour, tourData.Kilometre_Deb_Tour, tourData.Kilometre_Fin_Tour, tourData.Poids_Charge_Vehicule,
            tourData.Poids_Vide_Vehicule, tourData.poidsNet, tourData.Poids_ApresSousProduitsCat2, tourData.date_reception,
            tourData.heure_reception, tourData.Incident_De_Pesee, tourData.Date_enr, tourData.centre_traitmt, tourData.chauffeur1_horaire_debut,
            tourData.chauffeur1_horaire_fin, tourData.chauffeur2_horaire_debut, tourData.chauffeur2_horaire_fin]);
        console.log(query);
        connection.pool.query(query, (error, response) => {
            if (error) reject(error);
            console.log(response);
            resolve({ success: true, insertId: response.insertId });
        });
    });
}

/**
 * Updates a tour in the database.
 * @param {JSON} tourData The tour data.
 * @return {Promise} A promise of resolution.
 */
function updateTour(tourData) {
    return new Promise((resolve, reject) => {
        const updateQuery = 'UPDATE t_bon_onyx SET TIDTOUR = ?, num_bon = ?, vehicule = ?, Chauffeur_1 = ?, Chauffeur_2 = ?, '+
            'Heure_Deb_Tour = ?, Heure_Fin_Tour = ?, Kilometre_Deb_Tour = ?, Kilometre_Fin_Tour = ?, ' +
            'Poids_Charge_Vehicule = ?, Poids_Vide_Vehicule = ?, '+
            'poidsNet = ?, Poids_ApresSousProduitsCat2 = ?, date_reception = ?, heure_reception = ?, Incident_De_Pesee = ?, '+
            'Date_enr = ?, centre_traitmt = ?, chauffeur1_horaire_debut = ?, chauffeur1_horaire_fin = ?, chauffeur2_horaire_debut = ? '+
            'chauffeur2_horaire_fin = ? WHERE RefbonOnyx = ?';
        const query = mysql.format(updateQuery, [
            tourData.TIDTOUR, tourData.num_bon, tourData.vehicule, tourData.Chauffeur_1, tourData.Chauffeur_2, tourData.Heure_Deb_Tour,
            tourData.Heure_Fin_Tour, tourData.Kilometre_Deb_Tour, tourData.Kilometre_Fin_Tour, tourData.Poids_Charge_Vehicule,
            tourData.Poids_Vide_Vehicule, tourData.poidsNet, tourData.Poids_ApresSousProduitsCat2, tourData.date_reception,
            tourData.heure_reception, tourData.Incident_De_Pesee, tourData.Date_enr, tourData.centre_traitmt, tourData.chauffeur1_horaire_debut,
            tourData.chauffeur1_horaire_fin, tourData.chauffeur2_horaire_debut, tourData.chauffeur2_horaire_fin, tourData.RefbonOnyx]);
        connection.pool.query(query, (error, response) => {
            if (error) reject(error);
            resolve({ success: true, affectedRows: response.affectedRows });
        });
    });
}

/**
 * Deletes a tour from the database.
 * @param {Number} tourId The tour ID.
 * @return {Promise} A promise of resolution.
 */
function deleteTour(tourId) {
    return new Promise((resolve, reject) => {
        const deleteQuery = 'DELETE FROM t_bon_onyx WHERE RefbonOnyx = ?';
        const query = mysql.format(deleteQuery, [tourId]);
        connection.pool.query(query, (error, response) => {
            if (error) reject(error);
            resolve({ success: true, affectedRows: response.affectedRows });
        });
    });
}

export default {
    getAllTours,
    getTourById,
    getToursByDate,
    getCallsForTour,
    createTour,
    updateTour,
    deleteTour,
};
