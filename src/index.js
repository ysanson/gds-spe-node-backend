import 'dotenv/config';
import cors from 'cors';
import express from 'express';

import routes from './routes';

const app = express();

// Middlewares at application level
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routes
app.use('/api', routes);

// Start the application
app.listen(process.env.PORT, () =>
    console.log(`App listening on port ${process.env.PORT}`),
);
