import {Router} from 'express';
import loginController from '../controllers/loginController';

// eslint-disable-next-line new-cap
const router = Router();

router.post('/login', (req, res) => {
    loginController.loginUser(req.body)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.post('/logout', (req, res) => {

});

export default router;
