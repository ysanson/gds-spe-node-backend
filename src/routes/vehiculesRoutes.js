import {Router} from 'express';
import verifyToken from '../controllers/authentication';
import vehiculeController from '../controllers/vehiculeController';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    vehiculeController.getAllVehicules()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/:vId', (req, res) => {
    vehiculeController.getVehiculeById(req.params.vId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

export default router;
