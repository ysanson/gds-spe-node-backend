import {Router} from 'express';
import verifyToken from '../controllers/authentication';
import anomaliesController from '../controllers/anomaliesController';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    anomaliesController.getAnomalies()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/EDI', (req, res) => {
    anomaliesController.getEDIAnomalies()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

export default router;
