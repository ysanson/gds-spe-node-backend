import {Router} from 'express';
import conenlController from '../controllers/conenlController';
import verifyToken from '../controllers/authentication';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    conenlController.getAllCONENL()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/appel/:callId', (req, res) => {
    conenlController.getCONENLByCall(req.params.callId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.post('/appel', (req, res) => {
    conenlController.insertCONENLForCall(req.body)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/:cId', (req, res) => {
    conenlController.getCONENLById(req.params.cId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

export default router;
