import { Router } from 'express';
import verifyToken from '../controllers/authentication';
import usersController from '../controllers/usersController';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

// Gets all users
router.get('/', (req, res) => {
    usersController.getUsers((response, status) => {
        res.status(status).json(response);
    });
});

// Gets a specific user
router.get('/:userId', (req, res) => {
    const userId = req.params.userId;
    usersController.getUserById(userId, (response, status) => {
        res.status(status).json(response);
    });
});

// Creates a new user
router.post('/', (req, res) => {
    const userData = req.body;
    usersController.createUser(userData, (response, status) =>{
        res.status(status).json(response);
    });
});

// Updates a user with specific information
router.patch('/:userId', (req, res) => {
    const userId = req.params.userId;
    const changedData = req.body;
    if (userId == changedData.id) {
        usersController.updateUser(changedData)
            .then( (response) => res.status(response[1]).json(response[0]))
            .catch( (err) => res.status(500).json({ error: true, message: err }));
    } else {
        res.status(400).json({ error: true, message: 'Fields are missing' });
    }
});

// Deletes a user
router.delete('/:userId', (req, res) => {
    usersController.deleteUser(req.params.userId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({ error: true, message: err }));
});

// Updates a user password
router.patch('/newPassword/:userId', (req, res) => {
    usersController.updateUserPassword(req.body, req.params.userId, (response, status) => {
        res.status(status).json(response);
    });
});

export default router;
