import {Router} from 'express';
import verifyToken from '../controllers/authentication';
import natspanController from '../controllers/natspanController';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    natspanController.getAllNatspan()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/:nId', (req, res) => {
    natspanController.getNatspanById(req.params.nId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

export default router;
