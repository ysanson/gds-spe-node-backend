import {Router} from 'express';
import verifyToken from '../controllers/authentication';
import vetController from '../controllers/vetsController';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    vetController.getVets()
        .then( (response) => res.status(response[1]).json(response[0]) )
        .catch((err) => res.status(500).json({error: true, message: err}));
});

router.get('/:vId', (req, res) => {
    vetController.getVetById(req.params.vId)
        .then( (response) => res.status(response[1]).json(response[0]) )
        .catch((err) => res.status(500).json({error: true, message: err}));
});

router.post('/', (req, res) => {
    vetController.createVet(req.body)
        .then( (response) => res.status(response[1]).json(response[0]) )
        .catch((err) => res.status(500).json({error: true, message: err}));
});

router.patch('/:vId', (req, res) => {
    const newData = req.body;
    newData.vId = req.params.vId;
    vetController.updateVet(newData)
        .then( (response) => res.status(response[1]).json(response[0]) )
        .catch((err) => res.status(500).json({error: true, message: err}));
});

router.delete('/:vId', (req, res) => {
    vetController.deleteVet(req.params.vId)
        .then( (response) => res.status(response[1]).json(response[0]) )
        .catch((err) => res.status(500).json({error: true, message: err}));
});

export default router;
