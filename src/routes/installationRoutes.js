import {Router} from 'express';
import verifyToken from '../controllers/authentication';
import installationsController from '../controllers/installationsController';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    const searchParams = {};
    searchParams.ede = req.query.ede;
    searchParams.siret = req.query.siret;
    searchParams.poula = req.query.poula;
    searchParams.tva = req.query.tva;
    searchParams.dosequ = req.query.dosequ;
    searchParams.name = req.query.name;
    searchParams.zoneDom = req.query.zoneDom;
    searchParams.zoneExp = req.query.zoneExp;
    searchParams.PCDom = req.query.PCDom;
    searchParams.PCExp = req.query.PCExp;
    searchParams.active = req.query.active;
    installationsController.getInstallations(searchParams)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});


router.get('/:insId/animals', (req, res) => {
    const fromDate = req.query.from;
    const toDate = req.query.to;
    if (fromDate && toDate) {
        installationsController.getInstallationAnimals(fromDate, toDate, req.params.insId)
            .then( (response) => res.status(response[1]).json(response[0]))
            .catch( (err) => res.status(500).json({error: true, message: err}));
    } else {
        res.status(400).json({error: true, message: 'Dates are missing.'});
    }
});

router.get('/:insId', (req, res) => {
    installationsController.getInstallationById(req.params.insId)
        .then( (response) => res.status(response[1]).json(response[0]) )
        .catch((err) => res.status(500).json({error: true, message: err}));
});

router.post('/', (req, res) => {
    installationsController.addInstallation(req.body)
        .then( (response) => res.status(201).json(response))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.delete('/:insId', (req, res) => {
    installationsController.deleteInstallation(req.params.insId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.patch('/:insId', (req, res) => {
    const newData = req.body;
    newData.insId = req.params.insId;
    installationsController.updateInstallation(newData)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

export default router;
