import {Router} from 'express';
import citiesController from '../controllers/citiesController';
import verifyToken from '../controllers/authentication';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    const searchParams = {};
    searchParams.CP = req.query.CP;
    searchParams.ville = req.query.ville;
    searchParams.commune = req.query.commune;
    searchParams.numZone = req.query.numZone;
    searchParams.zone = req.query.zone;
    citiesController.getCities(searchParams)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/:cId', (req, res) => {
    citiesController.getCityById(req.params.cId)
        .then( (response) => res.status(response[1]).json(response[0]) )
        .catch((err) => res.status(500).json({error: true, message: err}));
});

router.post('/', (req, res) => {
    citiesController.addCity(req.body)
        .then( (response) => res.status(response[1]).json(response[0]) )
        .catch((err) => res.status(500).json({error: true, message: err}));
});

router.patch('/:cId', (req, res) => {
    const newData = req.body;
    newData.cId = req.params.cId;
    citiesController.updateCity(newData)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.delete('/:cId', (req, res) => {
    citiesController.deleteCity(req.params.cId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

export default router;
