import { Router } from 'express';
import users from './userRoutes';
import login from './loginRoutes';
import installations from './installationRoutes';
import cities from './citiesRoutes';
import corporations from './corporationsRoutes';
import vets from './vetsRoutes';
import calls from './callRoutes';
import moraspe from './moraspeRoutes';
import typdoc from './typdocRoutes';
import animals from './animalsRoutes';
import infid from './infidRoutes';
import race from './raceRoutes';
import natspan from './natspanRoutes';
import tour from './tourRoutes';
import driver from './driversRoutes';
import species from './speciesRoutes';
import ttygis from './ttygisRoutes';
import conenl from './conenlRoutes';
import planning from './planningRoutes';
import anomalies from './anomaliesRoutes';
import vehicules from './vehiculesRoutes';
import centers from './centersRoutes';
import deathCauses from './deathCausesRoutes';

// eslint-disable-next-line new-cap
const router = Router();

router.get('/api-status', (req, res) =>
    res.json({
        status: 'running',
    })
);

router.use('/users', users);
router.use('/login', login);
router.use('/installations', installations);
router.use('/cities', cities);
router.use('/corporations', corporations);
router.use('/vets', vets);
router.use('/calls', calls);
router.use('/moraspe', moraspe);
router.use('/typdoc', typdoc);
router.use('/animals', animals);
router.use('/infid', infid);
router.use('/races', race);
router.use('/natspan', natspan);
router.use('/tours', tour);
router.use('/drivers', driver);
router.use('/species', species);
router.use('/ttygis', ttygis);
router.use('/conenl', conenl);
router.use('/planning', planning);
router.use('/anomalies', anomalies);
router.use('/vehicules', vehicules);
router.use('/centers', centers);
router.use('/deathCauses', deathCauses);

export default router;
