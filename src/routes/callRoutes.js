import {Router} from 'express';
import callsController from '../controllers/callsController';
import verifyToken from '../controllers/authentication';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

// Gets all calls from this year or a specific year
router.get('/', (req, res) => {
    const year = req.query.year;
    callsController.getAllCalls(year)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/yesterday', (req, res) => {
    callsController.getCallsFromYesterday()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/two-days', (req, res) => {
    callsController.getCallsFromTwoDays()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/years', (req, res) => {
    callsController.getCallYears()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/removed', (req, res) => {
    callsController.getRemovalsFromPeriod(req.query.from, req.query.to)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/:callId', (req, res) => {
    callsController.getCallById(req.params.callId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.post('/', (req, res) => {
    callsController.createCall(req.body)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.patch('/:callId', (req, res) => {
    if (req.params.callId == req.body.NumAppel) {
        callsController.updateCall(req.body)
            .then( (response) => res.status(response[1]).json(response[0]))
            .catch( (err) => res.status(500).json({error: true, message: err}));
    } else {
        res.status(400).json({error: true, message: 'ID doesn\'t match'});
    }
});

export default router;
