import {Router} from 'express';
import verifyToken from '../controllers/authentication';
import infidController from '../controllers/infidController';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    const exceptCows = req.query.exceptCows || false;
    infidController.getAllINFID(exceptCows)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/specific', (req, res) => {
    infidController.getSpecificINFID()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/indic1', (req, res) => {
    infidController.getAllIndic1()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/indic1/:iId', (req, res) => {
    infidController.getIndic1ById(req.params.iId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/indic2', (req, res) => {
    infidController.getAllIndic2()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/indic2/:iId', (req, res) => {
    infidController.getIndic2ById(req.params.iId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/:iId', (req, res) => {
    infidController.getINFIDById(req.params.iId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

export default router;
