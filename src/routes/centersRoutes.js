import {Router} from 'express';
import centerController from '../controllers/centerController';
import verifyToken from '../controllers/authentication';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    centerController.getAllCenters()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/:cId', (req, res) => {
    centerController.getCenterById(req.params.cId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

export default router;
