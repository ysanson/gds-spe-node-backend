import {Router} from 'express';
import typdocController from '../controllers/typdocController';
import verifyToken from '../controllers/authentication';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    typdocController.getAllTypdoc()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/:tId', (req, res) => {
    typdocController.getTypdocById(req.params.tId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

export default router;
