import {Router} from 'express';
import verifyToken from '../controllers/authentication';
import corpController from '../controllers/corporationsController';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    corpController.getCorporations()
        .then( (response) => res.status(response[1]).json(response[0]) )
        .catch((err) => res.status(500).json({error: true, message: err}));
});

router.get('/:corpId', (req, res) => {
    corpController.getCorporationById(req.params.corpId)
        .then( (response) => res.status(response[1]).json(response[0]) )
        .catch((err) => res.status(500).json({error: true, message: err}));
});

router.post('/', (req, res) => {
    corpController.createCorporation(req.body)
        .then( (response) => res.status(response[1]).json(response[0]) )
        .catch((err) => res.status(500).json({error: true, message: err}));
});

router.patch('/:corpId', (req, res) => {
    const newData = req.body;
    newData.corpId = req.params.corpId;
    corpController.updateCorporation(newData)
        .then( (response) => res.status(response[1]).json(response[0]) )
        .catch((err) => res.status(500).json({error: true, message: err}));
});

router.delete('/:corpId', (req, res) => {
    corpController.deleteCorporation(req.params.corpId)
        .then( (response) => res.status(response[1]).json(response[0]) )
        .catch((err) => res.status(500).json({error: true, message: err}));
});

export default router;
