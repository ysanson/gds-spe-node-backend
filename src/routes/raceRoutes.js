import {Router} from 'express';
import verifyToken from '../controllers/authentication';
import raceController from '../controllers/racesController';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    const onlyCows = req.query.onlyCows || false;
    raceController.getAllRaces(onlyCows)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/:rId', (req, res) => {
    raceController.getRaceById(req.params.rId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

export default router;
