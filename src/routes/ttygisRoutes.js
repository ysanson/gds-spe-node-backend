import {Router} from 'express';
import verifyToken from '../controllers/authentication';
import ttygisController from '../controllers/ttygisController';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    ttygisController.getAllTTYGIS()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/:tId', (req, res) => {
    ttygisController.getTTYGISById(req.params.tId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

export default router;
