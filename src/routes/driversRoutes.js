import {Router} from 'express';
import verifyToken from '../controllers/authentication';
import driverController from '../controllers/driversController';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    driverController.getAllDrivers()
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

router.get('/:driverId', (req, res) => {
    driverController.getDriverById(req.params.driverId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({error: true, message: err}));
});

export default router;
