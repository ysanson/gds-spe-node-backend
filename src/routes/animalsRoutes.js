import { Router } from 'express';
import verifyToken from '../controllers/authentication';
import animalsController from '../controllers/animalsController';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

// Gets all animals or the ones from a specific call
router.get('/', (req, res)=> {
    const callId = req.query.NumAppel;
    const from = req.query.from;
    const to = req.query.to;
    animalsController.getAnimals(callId, from, to)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({ error: true, message: err }));
});

// Gets the data from cattle removed
router.get('/cattleReport', (req, res) => {
    animalsController.createCattleReport(req.query.from, req.query.to)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({ error: true, message: err }));
});

// Gets a specific animal
router.get('/:animalId', (req, res)=> {
    animalsController.getAnimalById(req.params.animalId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({ error: true, message: err }));
});


// Creates a new animal
router.post('/', (req, res) => {
    animalsController.createAnimal(req.body)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({ error: true, message: err }));
});

// Updates an animal
router.patch('/:animalId', (req, res) => {
    if (req.params.animalId == req.body.RefCollecte ) {
        animalsController.updateAnimal(req.body)
            .then( (response) => res.status(response[1]).json(response[0]))
            .catch( (err) => res.status(500).json({ error: true, message: err }));
    } else {
        res.status(400).json({ error: true, message: 'ID doesn\'t match' });
    }
});

export default router;
