import { Router } from 'express';
import verifyToken from '../controllers/authentication';
import tourController from '../controllers/toursController';

// eslint-disable-next-line new-cap
const router = Router();

router.use(verifyToken);

router.get('/', (req, res) => {
    const date = req.query.date;
    tourController.getAllTours(date)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({ error: true, message: err }));
});

router.get('/:tourId/calls', (req, res) => {
    tourController.getCallsForTour(req.params.tourId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({ error: true, message: err }));
});

router.get('/:tourId', (req, res) => {
    tourController.getTourById(req.params.tourId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({ error: true, message: err }));
});

router.post('/', (req, res) => {
    tourController.createTour(req.body)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({ error: true, message: err }));
});

router.patch('/:tourId', (req, res) => {
    if (req.params.tourId == req.body.RefbonOnyx) {
        tourController.updateTour(req.body)
            .then( (response) => res.status(response[1]).json(response[0]))
            .catch( (err) => res.status(500).json({ error: true, message: err }));
    } else {
        res.status(400).json({ error: true, message: 'ID doesn\'t match' });
    }
});

router.delete('/:tourId', (req, res) => {
    tourController.deleteTour(req.params.tourId)
        .then( (response) => res.status(response[1]).json(response[0]))
        .catch( (err) => res.status(500).json({ error: true, message: err }));
});

export default router;
