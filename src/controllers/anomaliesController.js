import mysql from '../database';

/**
 * Get normal anomalies from the database.
 * @return {Promise} A promise of resolution.
 */
function getAnomalies() {
    return new Promise((resolve, reject) => {
        mysql.anomaly.getAnomalies()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}
/**
 * Get the anomalies related to the EDI.
 * @return {Promise} A promise of resolution
 */
function getEDIAnomalies() {
    return new Promise((resolve, reject) => {
        mysql.anomaly.getEDIAnomalies()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

export default {
    getAnomalies,
    getEDIAnomalies,
};
