import mysql from '../database';

/**
 * Gets all the conenl from the database.
 * @return {Promise} A promise with the data.
 */
function getAllCONENL() {
    return new Promise((resolve, reject) => {
        mysql.conenl.getAllCONENL()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Gets the conenl by its id.
 * @param {Number} cId the id.
 * @return {Promise} A promise with the data.
 */
function getCONENLById(cId) {
    return new Promise((resolve, reject)=> {
        mysql.conenl.getCONENLById(cId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Gets the conenl for a given call.
 * @param {Number} callId the call ID.
 * @return {Promise} A promise with the data.
 */
function getCONENLByCall(callId) {
    return new Promise((resolve, reject) => {
        mysql.conenl.getCONENLForCall(callId)
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Inserts new conenl for a call.
 * @param {JSON} newData The new data.
 * @return {Promise} A promise of resolution.
 */
function insertCONENLForCall(newData) {
    return new Promise((resolve, reject) => {
        const data = [];
        for (const con of newData.CONENL) {
            data.push([newData.NumAppel, con]);
        }
        mysql.conenl.insertCONENLForCall(newData)
            .then( (dbRes) => resolve([dbRes, 201]))
            .catch( (error) => reject(error));
    });
}

export default {
    getAllCONENL,
    getCONENLById,
    getCONENLByCall,
    insertCONENLForCall,
};
