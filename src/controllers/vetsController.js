import mysql from '../database';

/**
 * Gets all the vets from the database.
 * @return {Promise} A promise of resolution.
 */
function getVets() {
    return new Promise((resolve, reject) => {
        mysql.vet.getAllVets()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * get a vet by ID.
 * @param {Number} vId The vet ID.
 * @return {Promise} A promise of resolution.
 */
function getVetById(vId) {
    return new Promise((resolve, reject) => {
        mysql.vet.getVetById(vId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Deletes a vet in the database.
 * @param {Number} vId The vet ID.
 * @return {Promise} A promise of resolution.
 */
function deleteVet(vId) {
    return new Promise((resolve, reject) => {
        mysql.vet.deleteVet(vId)
            .then( (dbRes) => {
                if (dbRes.affectedRows == 0) {
                    resolve([{ error: true, notFound: true }, 404]);
                } else {
                    resolve([dbRes, 200]);
                }
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Creates a vet in the database.
 * @param {JSON} vetData Yhe vet data.
 * @return {Promise} A promise of resolution.
 */
function createVet(vetData) {
    return new Promise((resolve, reject) => {
        const data = {};
        data['Noms-Prenoms'] = vetData['Noms-Prenoms'];
        data.cabinet = vetData.cabinet || '';
        data.Adresse = vetData.Adresse || '';
        data['Code Postal'] = vetData['Code Postal'] || '';
        data['Lieux-dits'] = vetData['Lieux-dits'] || '';
        data.Ville = vetData.Ville || '';
        data.Telephone = vetData.Telephone || '';
        data.Fax = vetData.Fax || '';
        if (data['Noms-Prenoms']) {
            mysql.vet.createVet(data)
                .then( (dbRes) => resolve([dbRes, 201]))
                .catch( (error) => reject(error));
        } else {
            resolve([{ error: true, message: 'Some information are missing.' }, 400]);
        }
    });
}

/**
 * Updates a vet in the database with the data provided.
 * @param {JSON} vetData The new vet data.
 * @return {Promise} A promise of resolution.
 */
function updateVet(vetData) {
    return new Promise((resolve, reject) => {
        mysql.vet.getVetById(vetData.vId)
            .then( (oldData) => {
                if (oldData.notFound) {
                    return oldData;
                } else {
                    const data = {};
                    data['Noms-Prenoms'] = vetData['Noms-Prenoms'] || oldData['Noms-Prenoms'];
                    data.cabinet = vetData.cabinet || oldData.cabinet;
                    data.Adresse = vetData.Adresse || oldData.Adresse;
                    data['Code Postal'] = vetData['Code Postal'] || oldData['Code Postal'];
                    data['Lieux-dits'] = vetData['Lieux-dits'] || oldData['Lieux-dits'];
                    data.Ville = vetData.Ville || oldData.Ville;
                    data.Telephone = vetData.Telephone || oldData.Telephone;
                    data.Fax = vetData.Fax || oldData.Fax;
                    data.vId = vetData.vId;
                    return mysql.vet.updateVet(data);
                }
            })
            .then( (dbRes) => {
                if (dbRes.error) {
                    resolve([{ error: true, notFound: true }, 404]);
                }
                if (dbRes.affectedRows == 0) {
                    resolve([{ error: true, message: 'The resource cannot be updated for now.' }, 503]);
                } else {
                    resolve([dbRes, 200]);
                }
            })
            .catch( (error) => reject(error));
    });
}

export default {
    getVets,
    getVetById,
    deleteVet,
    createVet,
    updateVet,
};
