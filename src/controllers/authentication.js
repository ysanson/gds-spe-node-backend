import 'dotenv/config';
import jwt from 'jsonwebtoken';

/**
 * Middleware function to verify the tokens.
 * @param {*} req the request
 * @param {*} res the result
 * @param {Function} next the callback
 * @return {*} a response to the client if the token is not present or bad, or continues if ok
 */
function verifyToken(req, res, next) {
    // check header or url parameters or post parameters for token
    // Express headers are auto converted to lowercase
    let token = req.headers['x-access-token'] || req.headers['authorization'];
    if (token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length);
    }
    if (!token) {
        return res.status(401).send({ auth: false, message: 'No token provided.' });
    }

    // verifies secret and checks exp
    jwt.verify(token, process.env.SECRET, function(err, decoded) {
        if (err) {
            return res.status(401).send({ auth: false, message: 'Failed to authenticate token. '+err });
        }
        // if everything is good, save to request for use in other routes
        req.userId = decoded.id;
        next();
    });
}

export default verifyToken;
