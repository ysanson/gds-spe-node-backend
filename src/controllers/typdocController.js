import mysql from '../database';

/**
 * Gets all the labels from the database.
 * @return {Promise} A promise with everything.
 */
function getAllTypdoc() {
    return new Promise((resolve, reject) => {
        mysql.typdoc.getAllTypdoc()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Gets a moraspe by its id.
 * @param {Number} mId the id.
 * @return {promise} A promise of resolution.
 */
function getTypdocById(mId) {
    return new Promise((resolve, reject) => {
        mysql.typdoc.getTypdocById(mId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

export default {
    getAllTypdoc,
    getTypdocById,
};
