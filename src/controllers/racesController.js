import mysql from '../database';

/**
 * Gets all the races from the database.
 * @param {Boolean} onlyCows If true, we search only for cows.
 * @return {Promise} A promise of resolution.
 */
function getAllRaces(onlyCows) {
    return new Promise((resolve, reject) => {
        if (onlyCows) {
            mysql.race.getCowRaces()
                .then( (dbRes) => resolve([dbRes, 200]))
                .catch( (error) => reject(error));
        } else {
            mysql.race.getAllRaces()
                .then( (dbRes) => resolve([dbRes, 200]))
                .catch( (error) => reject(error));
        }
    });
}

/**
 * Gets a race by its id.
 * @param {Number} rId The race ID.
 * @return {Promise} A promise with the data.
 */
function getRaceById(rId) {
    return new Promise((resolve, reject) => {
        mysql.race.getRaceById(rId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

export default {
    getAllRaces,
    getRaceById,
};
