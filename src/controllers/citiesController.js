import mysql from '../database';

/**
 * Gets all the cities in the database.
 * If filters are provided, they are taken into account.
 * @param {JSON} params The request parameters
 * @return {Promise} a promise with all the cities.
 */
function getCities(params) {
    return new Promise((resolve, reject) => {
        mysql.city.getCities(params)
            .then( (rows) => {
                if (rows[0] === undefined) {
                    resolve([{ error: true, notFound: true }, 404]);
                } else {
                    resolve([rows, 200]);
                }
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Gets a city by its ID.
 * @param {Number} cId the city ID.
 * @return {Promise} A promise of resolution containing the values.
 */
function getCityById(cId) {
    return new Promise((resolve, reject) => {
        mysql.city.getCityById(cId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}
/**
 * Adds a new city to the database.
 * @param {JSON} cityData The city data.
 * @return {Promise} A promise of completion.
 */
function addCity(cityData) {
    return new Promise((resolve, reject) => {
        const newData = {};
        newData.CP = cityData.Code_Postal;
        newData.ville = cityData.Ville;
        newData.lieuxDits = cityData.Lieux_dits || '';
        newData.commune = cityData.Commune;
        newData.numZone = cityData.numZone || 0;
        newData.zone = cityData.zone;
        if (newData.CP && newData.ville && newData.commune && newData.zone) {
            mysql.city.createCity(newData)
                .then( (dbRes) => resolve([dbRes, 201]))
                .catch( (error) => reject(error));
        } else {
            resolve([{ error: true, message: 'Fields are missing.' }, 400]);
        }
    });
}

/**
 * Updates a city with the new values in parameter.
 * @param {JSON} newData The new data.
 * @return {Promise} A promise of completion.
 */
function updateCity(newData) {
    return new Promise((resolve, reject) => {
        mysql.city.getCityById(newData.cId)
            .then( (oldData) => {
                if (oldData.notFound) {
                    return { error: true, notFound: true };
                } else {
                    const diffData = {};
                    diffData.CP = newData.Code_Postal || oldData.Code_Postal;
                    diffData.ville = newData.Ville || oldData.Ville;
                    diffData.lieuxDits = newData.Lieux_dits || oldData.Lieux_dits;
                    diffData.commune = newData.Commune || oldData.Commune;
                    diffData.numZone = newData.numZone || oldData.numZone;
                    diffData.zone = newData.zone || oldData.zone;
                    diffData.cId = newData.cId;
                    if (diffData.CP && diffData.ville && diffData.commune && diffData.zone) {
                        mysql.city.updateCity(diffData)
                            .then( (dbRes) => resolve([dbRes, 200]))
                            .catch( (error) => reject(error));
                    } else {
                        resolve([{ error: true, message: 'Fields are missing.' }, 400]);
                    }
                }
            })
            .then( (dbRes) => {
                if (dbRes.error) {
                    resolve([{ error: true, notFound: true }, 404]);
                }
                if (dbRes.affectedRows == 0) {
                    resolve([{ error: true, message: 'The resource cannot be updated for now.' }, 503]);
                } else {
                    resolve([dbRes, 200]);
                }
            })
            .catch( (err) => reject(err));
    });
}
/**
 * Deletes a city with its ID.
 * @param {Number} cId The city ID.
 * @return {Promise} A promise of completion.
 */
function deleteCity(cId) {
    return new Promise((resolve, reject) => {
        mysql.city.deleteCity(cId)
            .then( (dbRes) => {
                if (dbRes.affectedRows == 0) {
                    resolve([{ error: true, notFound: true }, 404]);
                } else {
                    resolve([dbRes, 200]);
                }
            })
            .catch( (err) => reject(err));
    });
}

export default {
    getCities,
    getCityById,
    addCity,
    updateCity,
    deleteCity,
};
