import mysql from '../database';

/**
 * Gets all the centers from the database.
 * @return {Promise} A promise of resolution.
 */
function getAllCenters() {
    return new Promise((resolve, reject) => {
        mysql.center.getAllCenters()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Gets a center by its ID.
 * @param {Number} cId The center ID
 * @return {Promise} A promise of resolution.
 */
function getCenterById(cId) {
    return new Promise((resolve, reject) => {
        mysql.center.getCenterById(cId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

export default {
    getAllCenters,
    getCenterById,
};
