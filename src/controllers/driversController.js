import mysql from '../database';

/**
 * Gets all drivers from the database.
 * @return {Promise} A promise with the data.
 */
function getAllDrivers() {
    return new Promise((resolve, reject) => {
        mysql.driver.getAllDrivers()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}
/**
 * Gets a driver from the database.
 * @param {Number} driverId The driver ID.
 * @return {Promise} A promise with the data.
 */
function getDriverById(driverId) {
    return new Promise((resolve, reject) => {
        mysql.driver.getDriverById(driverId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

export default {
    getAllDrivers,
    getDriverById,
};
