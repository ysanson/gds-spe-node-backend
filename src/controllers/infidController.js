import mysql from '../database';

/**
 * Gets all the infid from the database.
 * @param {Boolean} exceptCows Specifies if the result should include everything or not.
 * @return {Promise} A promise with everything.
 */
function getAllINFID(exceptCows) {
    return new Promise((resolve, reject) => {
        if (exceptCows) {
            mysql.infid.getINFIDWithoutBovine()
                .then( (dbRes) => resolve([dbRes, 200]))
                .catch( (error) => reject(error));
        } else {
            mysql.infid.getAllINFID()
                .then( (dbRes) => resolve([dbRes, 200]))
                .catch( (error) => reject(error));
        }
    });
}

/**
 * Gets the INFID with indic1, indic2 and the otherINFID.
 * @return {Promise} A promise with the three data separated.
 */
function getSpecificINFID() {
    return new Promise((resolve, reject) => {
        const result = {};
        mysql.infid.getINFIDWithoutBovine()
            .then( (dbRes) => {
                result.otherINFID = dbRes;
                return mysql.indic1.getAllIndic1();
            })
            .then( (dbRes) => {
                result.indic1 = dbRes;
                return mysql.indic2.getAllIndic2();
            })
            .then( (dbRes) => {
                result.indic2 = dbRes;
                resolve([result, 200]);
            })
            .catch((error) => reject(error));
    });
}

/**
 * Gets an infid by its ID.
 * @param {Number} iId The infid ID
 * @return {Promise} A promise with the data.
 */
function getINFIDById(iId) {
    return new Promise((resolve, reject) => {
        mysql.infid.getINFIDById(iId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Retrieves all the first indic from the database.
 * @return {Promise} A promise with the data.
 */
function getAllIndic1() {
    return new Promise((resolve, reject) => {
        mysql.indic1.getAllIndic1()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Gets the first indic by its ID.
 * @param {Number} iId  the ID.
 * @return {Promise} A promise with etherything.
 */
function getIndic1ById(iId) {
    return new Promise((resolve, reject) => {
        mysql.indic1.getIndic1ById(iId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Retrieves all the first indic from the database.
 * @return {Promise} A promise with the data.
 */
function getAllIndic2() {
    return new Promise((resolve, reject) => {
        mysql.indic2.getAllIndic2()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Gets the first indic by its ID.
 * @param {Number} iId  the ID.
 * @return {Promise} A promise with etherything.
 */
function getIndic2ById(iId) {
    return new Promise((resolve, reject) => {
        mysql.indic2.getIndic2ById(iId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

export default {
    getAllINFID,
    getINFIDById,
    getSpecificINFID,
    getAllIndic1,
    getIndic1ById,
    getAllIndic2,
    getIndic2ById,
};
