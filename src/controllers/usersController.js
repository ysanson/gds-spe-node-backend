import 'dotenv/config';
import mysql from '../database';
import bcrypt from 'bcrypt';

/**
 * Gets all the users from the database.
 * @param {Function} callback the function to call back.
 */
function getUsers(callback) {
    mysql.user.getUsers((dbVar) => {
        let status = 200;
        if (dbVar.error) {
            status = 500;
        }
        callback(dbVar, status);
    });
}

/**
 * Gets a user by its Id.
 * @param {Number} userId the user ID.
 * @param {*} callback the function to call back.
 */
function getUserById(userId, callback) {
    mysql.user.getUserById(userId)
        .then( (dbVar) => {
            let status = 200;
            if (dbVar.error) {
                status = 500;
            } else if (dbVar.notFound) {
                status = 404;
            }
            callback(dbVar, status);
        })
        .catch((err) => callback({ error: true, message: err }, 500));
}

/**
 * Creates a user based on the given parameters.
 * @param {JSON} userData The user data in JSON format.
 * @param {*} callback The function to call back.
 */
function createUser(userData, callback) {
    const firstname = userData.firstname || '';
    const lastname = userData.lastname || '';
    const pseudo = userData.pseudo;
    const password = userData.password;
    const role = userData.role;
    if (typeof pseudo !== 'undefined' && typeof password !== 'undefined' && role !== 'undefined') {
        bcrypt.hash(password, 10)
            .then((hashedPwd) => {
                mysql.user.createUser(firstname, lastname, pseudo, hashedPwd, role, (dbVar) => {
                    let status = 201;
                    if (dbVar.error) {
                        status = 500;
                    }
                    callback(dbVar, status);
                });
            });
    } else {
        callback({ error: true, message: 'Some information are missing.' }, 400);
    }
}

/**
 * Deletes a user in the database, given its ID.
 * @param {Number} userId the user id.
 * @return {Promise} A promise of completion.
 */
function deleteUser(userId) {
    return new Promise((resolve, reject) => {
        mysql.user.deleteUser(userId)
            .then( (dbVar) => {
                let status = 200;
                if (dbVar.error) {
                    status = 500;
                } else if (dbVar.affectedRows != 1) {
                    status = 409;
                }
                resolve([dbVar, status]);
            })
            .catch((error) => reject(error));
    });
}

/**
 * Updates a user with the changed data.
 * Data that can be changed:
 *  - firstname
 *  - lastname
 *  - pseudo
 *  - role
 * @param {JSON} changedData a JSON containing the data.
 * @return {Promise} the response.
 */
function updateUser(changedData) {
    return new Promise((resolve, reject) => {
        mysql.user.getUserById(changedData.id)
            .then((dbRes) => {
                if (dbRes.notFound) return dbRes;
                const newData = {
                    firstname: changedData.firstname || dbRes.firstname,
                    lastname: changedData.lastname || dbRes.lastname,
                    pseudo: changedData.pseudo || dbRes.pseudo,
                    role: changedData.role || dbRes.role,
                    userId: changedData.id,
                };
                return mysql.user.updateUser(newData);
            })
            .then((result) => {
                if (result.error) {
                    resolve([{ error: true, notFound: true }, 404]);
                }
                if (result.affectedRows == 0) {
                    resolve([{ error: true, message: 'The resource cannot be updated for now.' }, 503]);
                } else {
                    resolve([result, 200]);
                }
            })
            .catch((error) => reject(error));
    });
}

/**
 * Updates a user password, and verifies if the old one is the good one.
 * @param {JSON} userData JSON containing the old and the new passwords.
 * @param {Number} userId The user ID.
 * @param {Function} callback The function to call back.
 */
function updateUserPassword(userData, userId, callback) {
    const oldPwd = userData.oldPwd;
    const newPwd = userData.newPwd;
    const username = userData.pseudo;
    if (oldPwd && newPwd) {
        mysql.user.getPasswordFromUser(username)
            .then( (oldValues) => {
                if (oldValues.notFound) {
                    callback(oldValues, 404);
                } else {
                    if (!bcrypt.compareSync(oldPwd, oldValues.password)) {
                        callback({ error: true, message: 'Old password does not match.' }, 418);
                        return;
                    } else {
                        return bcrypt.hash(newPwd, 10);
                    }
                }
            })
            .then( (hashedPwd) => mysql.user.updatePassword(userId, hashedPwd))
            .then( (dbVar) => {
                let status = 200;
                if (dbVar.error) {
                    status = 500;
                } else if (dbVar.affectedRows != 1) {
                    status = 409;
                }
                callback(dbVar, status);
            })
            .catch( (err) => callback({ error: true, message: err }, 500));
    } else {
        callback({ error: true, message: 'Passwords not provided.' }, 400);
    }
}

export default {
    getUsers,
    getUserById,
    createUser,
    deleteUser,
    updateUser,
    updateUserPassword,
};
