import mysql from '../database/';

/**
 * Retrieves all the calls from the database.
 * @param {Number} year The year to search calls.
 * @return {Promise} A promise with all the calls.
 */
function getAllCalls(year) {
    return new Promise((resolve, reject) => {
        if (year) {
            mysql.call.getCallsByYear(year)
                .then( (dbRes) => resolve([dbRes, 200]))
                .catch( (error) => reject(error));
        } else {
            mysql.call.getAllCalls()
                .then( (dbRes) => resolve([dbRes, 200]))
                .catch( (error) => reject(error));
        }
    });
}

/**
 * Gets a call by its id.
 * @param {Number} callId The call ID.
 * @return {Promise} A promise containing all the call values.
 */
function getCallById(callId) {
    return new Promise((resolve, reject) => {
        mysql.call.getCallById(callId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Gets all the calls from yesterday to today.
 * @return {Promise} A promise with all the calls.
 */
function getCallsFromYesterday() {
    return new Promise((resolve, reject) => {
        mysql.call.getCallsFromYesterday()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Gets all the calls from two days to today.
 * @return {Promise} A promise of resolution.
 */
function getCallsFromTwoDays() {
    return new Promise((resolve, reject) => {
        mysql.call.getCallsFromTwoDays()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Gets all the years where there was a call.
 * @return {Promise} A promise with all the years.
 */
function getCallYears() {
    return new Promise((resolve, reject) => {
        mysql.call.getCallYears()
            .then( (dbRes) => {
                const years = [];
                for (const row of dbRes) {
                    if (row.year !== null && row.year > 2000) {
                        years.push(row.year);
                    }
                }
                resolve([years, 200]);
            })
            .catch( (error) => reject(error));
    });
}

/**
 * Creates a call in the database.
 * @param {JSON} data The call data passed by the client.
 * @return {Promise} A promise of resolution.
 */
function createCall(data) {
    return new Promise((resolve, reject) => {
        try {
            const callData = {
                PASSSAP: data.PASSSAP || null,
                TYPDOC: data.TYPDOC || 1,
                Date_Appel: data.Date_Appel ? new Date(data.Date_Appel) : undefined,
                Heure_Appel: data.Heure_Appel ? new Date(data.Heure_Appel) : undefined,
                TTYGIS: data.TTYGIS || 1,
                Id_Etablissement: data.Id_Etablissement,
                autorisation_veto: data.autorisation_veto || 0,
                DAB: data.DAB || 0,
                Remarques: data.Remarques || '',
                Num_Certificat: data.Num_Certificat || null,
                Date_Enlev: data.Date_Enlev ? new Date(data.Date_Enlev) : null,
                Heure_Enlev: data.Heure_Enlev ? new Date(data.Heure_Enlev) : null,
                Enlevement: data.Enlevement || 0,
                appel_tx_chauffeur: data.appel_tx_chauffeur || 0,
                annulation: data.annulation || 0,
                date_appel_resir: data.date_appel_resir || null,
                appel_resir: data.appel_resir || 0,
                dte_autori_veto: data.dte_autori_veto ? new Date(data.dte_autori_veto) : null,
                dte_appel_trans: data.dte_appel_trans ? new Date(data.dte_appel_trans) : null,
                appel_noneffec: data.appel_noneffec || -1,
                date_cocher_enlev: data.date_cocher_enlev || null,
                centre_trait_bon: data.centre_trait_bon || null,
                appel_auto: data.appel_auto || 0,
                autopsie: data.autopsie || null,
                RefBonCtrait: data.RefBonCtrait || null,
                MORASPE: data.MORASPE || 1,
            };
            callData.date_noneffec = data.date_noneffec || callData.appel_noneffec === -1 ? new Date() : null;
            if (callData.Date_Appel && callData.Heure_Appel && callData.Id_Etablissement) {
                mysql.call.createCall(callData)
                    .then( (dbRes) => resolve([dbRes, 201]))
                    .catch( (error) => resolve(error));
            } else {
                resolve([{ error: true, message: 'Fields are missing.' }, 400]);
            }
        } catch (error) {
            reject(error);
        }
    });
}

/**
 * Updates a call in the database.
 * @param {JSON} data The new data
 * @return {Promise} A promise with success or error.
 */
function updateCall(data) {
    return new Promise((resolve, reject) => {
        mysql.call.getCallById(data.NumAppel)
            .then( (oldData) => {
                if (oldData.notFound) return oldData;
                const newData = {
                    PASSSAP: data.PASSSAP || oldData.PASSSAP,
                    TYPDOC: data.TYPDOC || oldData.TYPDOC,
                    Date_Appel: data.Date_Appel ? new Date(data.Date_Appel) : oldData.Date_Appel,
                    Heure_Appel: data.Heure_Appel ? new Date(data.Heure_Appel) : oldData.Heure_Appel,
                    TTYGIS: data.TTYGIS || oldData.TTYGIS,
                    Id_Etablissement: data.Id_Etablissement || oldData.Id_Etablissement,
                    autorisation_veto: data.autorisation_veto || oldData.autorisation_veto,
                    DAB: data.DAB || oldData.DAB,
                    Remarques: data.Remarques || oldData.Remarques,
                    Num_Certificat: data.Num_Certificat || oldData.Num_Certificat,
                    Date_Enlev: data.Date_Enlev ? new Date(data.Date_Enlev) : oldData.Date_Enlev,
                    Heure_Enlev: data.Heure_Enlev ? new Date(data.Heure_Enlev) : oldData.Heure_Enlev,
                    Enlevement: data.Enlevement || oldData.Enlevement,
                    appel_tx_chauffeur: data.appel_tx_chauffeur || oldData.appel_tx_chauffeur,
                    annulation: data.annulation || oldData.annulation,
                    date_appel_resir: data.date_appel_resir || oldData.date_appel_resir,
                    appel_resir: data.appel_resir || oldData.appel_resir,
                    dte_autori_veto: data.dte_autori_veto ? new Date(data.dte_autori_veto) : oldData.dte_autori_veto,
                    dte_appel_trans: data.dte_appel_trans ? new Date(data.dte_appel_trans) : oldData.dte_appel_trans,
                    appel_noneffec: data.appel_noneffec || oldData.appel_noneffec,
                    date_noneffec: data.date_noneffec || oldData.date_noneffec,
                    date_cocher_enlev: data.date_cocher_enlev || oldData.date_cocher_enlev,
                    centre_trait_bon: data.centre_trait_bon || oldData.centre_trait_bon,
                    appel_auto: data.appel_auto || oldData.appel_auto,
                    autopsie: data.autopsie || oldData.autopsie,
                    RefBonCtrait: data.RefBonCtrait || oldData.RefBonCtrait,
                    MORASPE: data.MORASPE || oldData.MORASPE,
                    NumAppel: data.NumAppel,
                };
                return mysql.call.updateCall(newData);
            })
            .then( (dbRes) => {
                if (dbRes.error) {
                    resolve([{ error: true, notFound: true }, 404]);
                }
                if (dbRes.affectedRows == 0) {
                    resolve([{ error: true, message: 'The resource cannot be updated for now.' }, 503]);
                } else {
                    resolve([dbRes, 200]);
                }
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Gets the calls taken between a period.
 * @param {String} from The start date, in ISO format
 * @param {String} to The end date, in ISO format
 * @return {Promise} A promise with the data
 */
function getRemovalsFromPeriod(from, to) {
    return new Promise((resolve, reject) => {
        if (from && to) {
            const fromDate = new Date(from);
            const toDate = new Date(to);
            mysql.call.getRemovalsFromPeriod(fromDate, toDate)
                .then( (dbRes) => resolve([dbRes, 200]))
                .catch( (error) => reject(error));
        } else {
            resolve([{ error: true, message: 'Dates are missing' }, 400]);
        }
    });
}

export default {
    getAllCalls,
    getCallById,
    getCallsFromYesterday,
    getCallsFromTwoDays,
    getCallYears,
    createCall,
    updateCall,
    getRemovalsFromPeriod,
};
