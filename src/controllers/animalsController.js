import mysql from '../database';

/**
 * Gets eiher all animals or just the ones from a particular call.
 * @param {Number} callId The call ID (undefined if any)
 * @param {String} from The from data range in ISO format
 * @param {String} to The To date range in ISO format
 * @return {Promise} A promise with all the animals inside.
 */
function getAnimals(callId, from, to) {
    return new Promise((resolve, reject) => {
        if (callId) {
            mysql.animal.getAnimalsByCall(callId)
                .then((dbRes) => resolve([dbRes, 200]))
                .catch((error) => reject(error));
        } else if (from && to) {
            const fromDate = new Date(from);
            const toDate = new Date(to);
            mysql.animal.getAnimalsFromPeriod(fromDate, toDate)
                .then((dbRes) => resolve([dbRes, 200]))
                .catch((error) => reject(error));
        } else {
            mysql.animal.getAllAnimals()
                .then((dbRes) => resolve([dbRes, 200]))
                .catch((error) => reject(error));
        }
    });
}

/**
 * Gets an animal by its ID.
 * @param {Number} animalId The animal ID.
 * @return {Promise} A promise with the animal data.
 */
function getAnimalById(animalId) {
    return new Promise((resolve, reject) => {
        mysql.animal.getAnimalByID(animalId)
            .then((dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch((err) => reject(err));
    });
}

/**
 * Creates an animal in the database.
 * @param {JSON} animalData The new data.
 * @return {Promise} A promise with the inserted ID.
 */
function createAnimal(animalData) {
    return new Promise((resolve, reject) => {
        const data = {
            Espece: animalData.Espece,
            CATMAT: animalData.CATMAT || 1,
            Race: animalData.Race || null,
            NATSPAN: animalData.NATSPAN,
            ESESPOI: animalData.ESESPOI || 2,
            TYPROD: animalData.TYPROD || null,
            SPE: animalData.SPE || 1,
            Sexe: animalData.Sexe || null,
            Nbre: animalData.Nbre || null,
            Poids: animalData.Poids || null,
            POIEF: animalData.POIEF || null,
            CodePaysNumAnimal: animalData.CodePaysNumAnimal || null,
            Identification_BV: animalData.Identification_BV || null,
            PasDIdentifiant: animalData.PasDIdentifiant || 0,
            Indicateur1: animalData.Indicateur1 || null,
            Indicateur2: animalData.Indicateur2 || 0,
            Cause_Mort: animalData.Cause_Mort || null,
            DAB: animalData.DAB || 0,
            congele: animalData.congele || 0,
            NonEnlevement: animalData.NonEnlevement || 0,
            NumNonEnlevent: animalData.NumNonEnlevent || null,
            NoteNonEnlevement: animalData.NoteNonEnlevement || null,
            NumAppel: animalData.NumAppel,
            DteNaiss: animalData.DteNaiss ? new Date(animalData.DteNaiss) : null,
            NumDAB: animalData.NumDAB || null,
            NumASDA: animalData.NumASDA || null,
            RefCauseMort: animalData.RefCauseMort || null,
            CircMort: animalData.CircMort || null,
        };
        if (data.Espece && data.NATSPAN && data.NumAppel) {
            mysql.animal.createAnimal(data)
                .then((dbRes) => resolve([dbRes, 201]))
                .catch((error) => reject(error));
        } else {
            resolve([{ error: true, message: 'Fields are missing.' }, 400]);
        }
    });
}

/**
 * Updates an animal in the database.
 * @param {JSON} animalData The new data.
 * @return {Promise} A promise of resolution.
 */
function updateAnimal(animalData) {
    return new Promise((resolve, reject) => {
        mysql.animal.getAnimalByID(animalData.RefCollecte)
            .then((oldData) => {
                if (oldData.notFound) return oldData;
                const newData = {
                    Espece: animalData.Espece || oldData.ESPECE,
                    CATMAT: animalData.CATMAT || oldData.CATMAT,
                    Race: animalData.Race || oldData.Race,
                    NATSPAN: animalData.NATSPAN || oldData.NATSPAN,
                    ESESPOI: animalData.ESESPOI || oldData.ESESPOI,
                    TYPROD: animalData.TYPROD || oldData.TYPROD,
                    SPE: animalData.SPE || oldData.SPE,
                    Sexe: animalData.Sexe || oldData.Sexe,
                    Nbre: animalData.Nbre || oldData.Nbre,
                    Poids: animalData.Poids || oldData.Poids,
                    POIEF: animalData.POIEF || oldData.POIEF,
                    CodePaysNumAnimal: animalData.CodePaysNumAnimal || oldData.CodePaysNumAnimal,
                    Identification_BV: animalData.Identification_BV || oldData.Identification_BV,
                    PasDIdentifiant: animalData.PasDIdentifiant || oldData.PasDIdentifiant,
                    Indicateur1: animalData.Indicateur1 || oldData.Indicateur1,
                    Indicateur2: animalData.Indicateur2 || oldData.Indicateur2,
                    Cause_Mort: animalData.Cause_Mort || oldData.Cause_Mort,
                    DAB: 0,
                    congele: animalData.congele || oldData.congele,
                    NonEnlevement: animalData.NonEnlevement || oldData.NonEnlevement,
                    NumNonEnlevent: animalData.NumNonEnlevent || oldData.NumNonEnlevent,
                    NoteNonEnlevement: animalData.NoteNonEnlevement || oldData.NoteNonEnlevement,
                    NumAppel: animalData.NumAppel || oldData.NumAppel,
                    DteNaiss: animalData.DteNaiss ? new Date(animalData.DteNaiss) : oldData.DteNaiss,
                    NumDAB: animalData.NumDAB || oldData.NumDAB,
                    NumASDA: animalData.NumASDA || oldData.NumASDA,
                    RefCauseMort: animalData.RefCauseMort || null,
                    CircMort: animalData.CircMort || null,
                    RefCollecte: animalData.RefCollecte,
                };
                return mysql.animal.updateAnimal(newData);
            })
            .then((dbRes) => {
                if (dbRes.error) {
                    resolve([{ error: true, notFound: true }, 404]);
                }
                if (dbRes.affectedRows == 0) {
                    resolve([{ error: true, message: 'The resource cannot be updated for now.' }, 503]);
                } else {
                    resolve([dbRes, 200]);
                }
            })
            .catch((err) => reject(err));
    });
}

/**
 * Gets the cattle that has been removed from a specific period.
 * @param {String} from The date to search from, in ISO Format
 * @param {String} to The date to search to, in ISO Format
 * @return {Promise} A promise with the cattle removed from a period
 */
function createCattleReport(from, to) {
    return new Promise((resolve, reject) => {
        if (from && to) {
            const fromDate = new Date(from);
            const toDate = new Date(to);
            mysql.animal.getCattleRemovalsData(fromDate, toDate)
                .then((Response) => resolve([Response, 200]))
                .catch((error) => reject(error));
        } else resolve([{ error: true, message: 'From and to are missing' }, 400]);
    });
}

export default {
    getAnimals,
    getAnimalById,
    createAnimal,
    updateAnimal,
    createCattleReport,
};
