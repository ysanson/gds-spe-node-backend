import mysql from '../database';

/**
 * Gets the corporations from the database.
 * @return {Promise} A promise of resolution.
 */
function getCorporations() {
    return new Promise((resolve, reject) => {
        mysql.corporation.getCorporations()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Gets a corporation by id.
 * @param {Number} corpId The corporation id.
 * @return {Promise} A promise of resolution.
 */
function getCorporationById(corpId) {
    return new Promise((resolve, reject) => {
        mysql.corporation.getCorporationById(corpId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Deletes a corporation by its id.
 * @param {Number} corpId the corp id.
 * @return {Promise} A promise of resolution.
 */
function deleteCorporation(corpId) {
    return new Promise((resolve, reject) => {
        mysql.corporation.deleteCorporation(corpId)
            .then( (dbRes) => {
                if (dbRes.affectedRows == 0) {
                    resolve([{ error: true, notFound: true }, 404]);
                } else {
                    resolve([dbRes, 200]);
                }
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Creates a corporation by its id.
 * @param {JSON} newData The new data.
 * @return {Promise} A promise of resolution.
 */
function createCorporation(newData) {
    return new Promise((resolve, reject) => {
        const data = {};
        data.nom_filiere = newData.nom_filiere;
        data.espece = newData.filiere || '';
        data.adresse = newData.adresse || '';
        data.fax = newData.fax || '';
        if (data.nom_filiere) {
            mysql.corporation.createCorporation(data)
                .then( (dbRes) => resolve([dbRes, 201]))
                .catch( (error) => reject(error));
        } else {
            resolve([{ error: true, message: 'Some information are missing.' }, 400]);
        }
    });
}

/**
 * Updates a corporation by its id.
 * @param {JSON} newData The updated data.
 * @return {Promise} A promise of resolution.
 */
function updateCorporation(newData) {
    return new Promise((resolve, reject) => {
        mysql.corporation.getCorporationById(newData.corpId)
            .then( (oldData) => {
                if (oldData.notFound) {
                    return oldData;
                } else {
                    const diffData = {};
                    diffData.nom_filiere = newData.nom_filiere || oldData.nom_filiere;
                    diffData.espece = newData.espece || oldData.espece;
                    diffData.adresse = newData.adresse || oldData.adresse;
                    diffData.fax = newData.fax || oldData.fax;
                    return mysql.corporation.updateCorporation(diffData);
                }
            })
            .then( (dbRes) => {
                if (dbRes.error) {
                    resolve([{ error: true, notFound: true }, 404]);
                }
                if (dbRes.affectedRows == 0) {
                    resolve([{ error: true, message: 'The resource cannot be updated for now.' }, 503]);
                } else {
                    resolve([dbRes, 200]);
                }
            })
            .catch( (err) => reject(err));
    });
}

export default {
    getCorporations,
    getCorporationById,
    deleteCorporation,
    createCorporation,
    updateCorporation,
};
