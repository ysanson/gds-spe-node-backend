import mysql from '../database';

/**
 * Gets all the vehicules from the database.
 * @return {promise} A promise with the data.
 */
function getAllVehicules() {
    return new Promise((resolve, reject) => {
        mysql.vehicule.getAllVehicules()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Gets a vehicule by its ID.
 * @param {Number} vId The vehicule ID.
 * @return {Promise} A promise of resolution with the data.
 */
function getVehiculeById(vId) {
    return new Promise((resolve, reject) => {
        mysql.vehicule.getVehicleById(vId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

export default {
    getAllVehicules,
    getVehiculeById,
};
