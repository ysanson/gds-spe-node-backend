import mysql from '../database';

/**
 * Gets all the ttygis from the database.
 * @return {Promise} A promise of resolution.
 */
function getAllTTYGIS() {
    return new Promise((resolve, reject) => {
        mysql.ttygis.getAllTTYGIS()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Gets a ttygis by its ID.
 * @param {Number} tId The ID.
 * @return {Promise} A promise of resolution.
 */
function getTTYGISById(tId) {
    return new Promise((resolve, reject)=> {
        mysql.ttygis.getTTYGISById(tId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

export default {
    getAllTTYGIS,
    getTTYGISById,
};
