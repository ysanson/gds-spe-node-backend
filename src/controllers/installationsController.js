import mysql from '../database';

/**
 * Gets all the installations in the database.
 * If filters are provided, they are taken into account.
 * @param {JSON} params The request parameters
 * @return {Promise} a promise with all the installations.
 */
function getInstallations(params) {
    return new Promise((resolve, reject) => {
        mysql.installation.getAllInstallations(params)
            .then( (rows) => {
                if (rows[0] === undefined) {
                    resolve([{ error: true, notFound: true }, 404]);
                } else {
                    resolve([rows, 200]);
                }
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Gets an installation by its ID.
 * @param {Number} insId the installation ID.
 * @return {Promise} A promise of resolution containing the values.
 */
function getInstallationById(insId) {
    return new Promise((resolve, reject) => {
        mysql.installation.getInstallationByID(insId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Gets the animals for an installation.
 * Caters to a period of time.
 * @param {String} fromDate The date to start searching.
 * @param {String} toDate The date to end searching.
 * @param {*} insId The installation ID.
 * @return {Promise} A promise with the data.
 */
function getInstallationAnimals(fromDate, toDate, insId) {
    return new Promise((resolve, reject) => {
        const fDate = new Date(fromDate);
        const tDate = new Date(toDate);
        if (insId) {
            mysql.installation.getInstallationAnimals(fDate, tDate, insId)
                .then( (dbRes) => resolve([dbRes, 200]))
                .catch( (error) => reject(error));
        } else {
            resolve([{ error: true, message: 'Bad request' }, 400]);
        }
    });
}

/**
 * Adds an installation to the database.
 * @param {JSON} insData The new installation parameters.
 * @return {Promise} A promise of completion.
 */
function addInstallation(insData) {
    return new Promise((resolve, reject) => {
        const newData = {
            ede: insData.EDEV2 || '',
            siret: insData.SIRETV2 || '',
            npac: insData.NPAC || '',
            poula: insData.POULA || '',
            tva: insData.TVA || '',
            dosequ: insData.DOSEQU || '',
            napi: insData.NAPI || '',
            typeAtelier: insData.Type_Atelier || '',
            detName: insData.Nom_Det || '',
            detFirstname: insData.Prenom_Det || '',
            domAddr: insData.Adrs_Dom || '',
            domNum: insData.num_voie_dom || '',
            domName: insData.nom_voie_dom || '',
            domExt: insData.extension_voie_dom || '',
            lieuDitDom: insData.Lieu_Dit_Dom || '',
            cityDom: insData.Ville_Dom || '',
            postalCodeDom: insData.CP_Dom || '',
            commDom: insData.Commune_Dom || '',
            sectorDom: insData.Secteur_dom || '',
            zoneDom: insData.Zone_Dom || '',
            tel1: insData.Telephone1 || '',
            tel2: insData.Portable1 || '',
            tel3: insData.Portable2 || '',
            vet: insData.nom_veto || '',
            frat: insData.fraterie || '',
            filiere: insData.filiere || null,
            actif: insData.actif || 1,
        };
        mysql.installation.createInstallation(newData)
            .then( (dbRes) => resolve(dbRes))
            .catch( (error) => reject(error));
    });
}

/**
 * Deletes an installation with it's ID.
 * @param {Number} insId  The installation ID.
 * @return {Promise}  A promise of completion.
 */
function deleteInstallation(insId) {
    return new Promise((resolve, reject) => {
        mysql.installation.deleteInstallation(insId)
            .then( (dbRes) => {
                if (dbRes.affectedRows == 0) {
                    resolve([{ error: true, notFound: true }, 404]);
                } else {
                    resolve([dbRes, 200]);
                }
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Updates an installation with the new data provided.
 * @param {JSON} newData The new data.
 * @return {Promise} A promise of completion.
 */
function updateInstallation(newData) {
    return new Promise((resolve, reject) => {
        mysql.installation.getInstallationByID(newData.insId)
            .then( (oldData) => {
                if (oldData.notFound) {
                    return { error: true, notFound: true };
                } else {
                    const diffData = {};
                    diffData.ede = newData.EDEV2 || oldData.EDEV2;
                    diffData.siret = newData.SIRETV2 || oldData.SIRETV2;
                    diffData.npac = newData.NPAC || oldData.NPAC;
                    diffData.poula = newData.POULA || oldData.POULA;
                    diffData.tva = newData.TVA || oldData.TVA;
                    diffData.dosequ = newData.DOSEQU || oldData.DOSEQU;
                    diffData.napi = newData.NAPI || oldData.NAPI;
                    diffData.typeAtelier = newData.Type_Atelier || oldData.Type_Atelier;
                    diffData.detName = newData.Nom_Det || oldData.Nom_Det;
                    diffData.detFirstname = newData.Prenom_Det || oldData.Prenom_Det;
                    diffData.domAddr = newData.Adrs_Dom || oldData.Adrs_Dom;
                    diffData.domNum = newData.num_voie_dom || oldData.num_voie_dom;
                    diffData.domName = newData.nom_voie_dom || oldData.nom_voie_dom;
                    diffData.domExt = newData.extension_voie_dom || oldData.extension_voie_dom;
                    diffData.lieuDitDom = newData.Lieu_Dit_Dom || oldData.Lieu_Dit_Dom;
                    diffData.cityDom = newData.Ville_Dom || oldData.Ville_Dom;
                    diffData.postalCodeDom = newData.CP_Dom || oldData.CP_Dom;
                    diffData.commDom = newData.Commune_Dom || oldData.Commune_Dom;
                    diffData.sectorDom = newData.Secteur_dom || oldData.Secteur_dom;
                    diffData.zoneDom = newData.Zone_Dom || oldData.Zone_Dom;
                    diffData.tel1 = newData.Telephone1 || oldData.Telephone1;
                    diffData.tel2 = newData.Portable1 || oldData.Portable1;
                    diffData.tel3 = newData.Portable2 || oldData.Portable2;
                    diffData.vet = newData.nom_veto || oldData.nom_veto;
                    diffData.frat = newData.fraterie || oldData.fraterie;
                    diffData.filiere = newData.filiere || oldData.filiere;
                    diffData.actif = newData.actif || oldData.actif;
                    diffData.insId = newData.insId;
                    return mysql.installation.updateInstallation(diffData);
                }
            })
            .then( (dbRes) => {
                if (dbRes.error) {
                    resolve([{ error: true, notFound: true }, 404]);
                }
                if (dbRes.affectedRows == 0) {
                    resolve([{ error: true, message: 'The resource cannot be updated for now.' }, 503]);
                } else {
                    resolve([dbRes, 200]);
                }
            })
            .catch( (err) => reject(err));
    });
}

export default {
    getInstallations,
    getInstallationById,
    getInstallationAnimals,
    addInstallation,
    deleteInstallation,
    updateInstallation,
};
