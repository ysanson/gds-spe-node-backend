import mysql from '../database';

/**
 * Gets all the tours from the database.
 * @param {String} tourDate Optional: the tour date to select.
 * @return {Promise} A promise with the data.
 */
function getAllTours(tourDate) {
    return new Promise((resolve, reject) => {
        if (tourDate) {
            if (/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/.test(tourDate)) {
                mysql.tour.getToursByDate(tourDate)
                    .then( (dbRes) => resolve([dbRes, 200]))
                    .catch( (error) => reject(error));
            } else {
                resolve([{ error: true, message: 'Invalid date format' }, 400]);
            }
        } else {
            mysql.tour.getAllTours()
                .then( (dbRes) => resolve([dbRes, 200]))
                .catch( (error) => reject(error));
        }
    });
}

/**
 * Gets a tour by its ID.
 * @param {Number} tourId The tour ID.
 * @return {Promise} A promise of resolution.
 */
function getTourById(tourId) {
    return new Promise((resolve, reject) => {
        mysql.tour.getTourById(tourId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Retrieves the calls for a specific tour.
 * @param {Number} tourId The tour ID.
 * @return {Promise} A promise of resolution
 */
function getCallsForTour(tourId) {
    return new Promise((resolve, reject) => {
        mysql.tour.getCallsForTour(tourId)
            .then( (dbRes) => {
                if (dbRes.notFound) resolve([dbRes, 404]);
                else resolve([dbRes, 200]);
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Creates a tour in the database.
 * @param {JSON} tourData The new data.
 * @return {Promise} A promise of resolution.
 */
function createTour(tourData) {
    return new Promise((resolve, reject) => {
        const data = {
            TIDTOUR: tourData.TIDTOUR,
            num_bon: tourData.num_bon || null,
            vehicule: tourData.vehicule,
            Chauffeur_1: tourData.Chauffeur_1 || null,
            Chauffeur_2: tourData.Chauffeur_2 || null,
            Heure_Deb_Tour: tourData.Heure_Deb_Tour ? new Date(tourData.Heure_Deb_Tour) : null,
            Heure_Fin_Tour: tourData.Heure_Fin_Tour ? new Date(tourData.Heure_Fin_Tour).toTimeString().substring(0, 5) : null,
            Kilometre_Deb_Tour: tourData.Kilometre_Deb_Tour || null,
            Kilometre_Fin_Tour: tourData.Kilometre_Fin_Tour || null,
            Poids_Charge_Vehicule: tourData.Poids_Charge_Vehicule || null,
            Poids_Vide_Vehicule: tourData.Poids_Vide_Vehicule || null,
            poidsNet: tourData.poidsNet || null,
            Poids_ApresSousProduitsCat2: tourData.Poids_ApresSousProduitsCat2 || null,
            date_reception: tourData.date_reception ? new Date(tourData.date_reception) : null,
            heure_reception: tourData.heure_reception ? new Date(tourData.heure_reception) : null,
            Incident_De_Pesee: tourData.Incident_De_Pesee || null,
            Date_enr: tourData.Date_enr || null,
            centre_traitmt: tourData.centre_traitmt || 2,
            chauffeur1_horaire_debut: tourData.chauffeur1_horaire_debut ? new Date(tourData.chauffeur1_horaire_debut) : null,
            chauffeur1_horaire_fin: tourData.chauffeur1_horaire_fin ? new Date(tourData.chauffeur1_horaire_fin) : null,
            chauffeur2_horaire_debut: tourData.chauffeur2_horaire_debut ? new Date(tourData.chauffeur2_horaire_debut) : null,
            chauffeur2_horaire_fin: tourData.chauffeur2_horaire_fin ? new Date(tourData.chauffeur2_horaire_fin) : null,
        };
        if (data.vehicule && data.centre_traitmt && data.TIDTOUR) {
            console.log(data);
            mysql.tour.createTour(data)
                .then( (dbRes) => resolve([dbRes, 201]))
                .catch( (error) => reject(error));
        } else {
            resolve([{ error: true, message: 'TIDTOUR, vehicule and centre_traitmt cannot be null' }, 400]);
        }
    });
}

/**
 * Updates a tour in the database.
 * @param {JSON} tourData The new data.
 * @return {Promise} A promise of resolution.
 */
function updateTour(tourData) {
    return new Promise((resolve, reject) => {
        mysql.tour.getTourById(tourData.RefbonOnyx)
            .then( (oldData) => {
                if (oldData.notFound) return oldData;
                const data = {
                    TIDTOUR: tourData.TIDTOUR || oldData.TIDTOUR,
                    num_bon: tourData.num_bon || oldData.num_bon,
                    vehicule: tourData.vehicule || oldData.vehicule,
                    Chauffeur_1: tourData.Chauffeur_1 || oldData.Chauffeur_1,
                    Chauffeur_2: tourData.Chauffeur_2 || oldData.Chauffeur_2,
                    Heure_Deb_Tour: tourData.Heure_Deb_Tour ? new Date(tourData.Heure_Deb_Tour) : oldData.Heure_Deb_Tour,
                    Heure_Fin_Tour: tourData.Heure_Fin_Tour
                        ? new Date(tourData.Heure_Fin_Tour).toTimeString().substring(0, 5)
                        : oldData.Heure_Fin_Tour,
                    Kilometre_Deb_Tour: tourData.Kilometre_Deb_Tour || oldData.Kilometre_Deb_Tour,
                    Kilometre_Fin_Tour: tourData.Kilometre_Fin_Tour || oldData.Kilometre_Fin_Tour,
                    Poids_Charge_Vehicule: tourData.Poids_Charge_Vehicule || oldData.Poids_Charge_Vehicule,
                    Poids_Vide_Vehicule: tourData.Poids_Vide_Vehicule || oldData.Poids_Vide_Vehicule,
                    poidsNet: tourData.poidsNet || oldData.poidsNet,
                    Poids_ApresSousProduitsCat2: tourData.Poids_ApresSousProduitsCat2 || oldData.Poids_ApresSousProduitsCat2,
                    date_reception: tourData.date_reception ? new Date(tourData.date_reception) : oldData.date_reception,
                    heure_reception: tourData.heure_reception ? new Date(tourData.heure_reception) : oldData.heure_reception,
                    Incident_De_Pesee: tourData.Incident_De_Pesee || oldData.Incident_De_Pesee,
                    Date_enr: tourData.Date_enr || oldData.Date_enr,
                    centre_traitmt: tourData.centre_traitmt || oldData.centre_traitmt,
                    chauffeur1_horaire_debut: tourData.chauffeur1_horaire_debut ?
                        new Date(tourData.chauffeur1_horaire_debut) : oldData.chauffeur1_horaire_debut,
                    chauffeur1_horaire_fin: tourData.chauffeur1_horaire_fin ?
                        new Date(tourData.chauffeur1_horaire_fin) : oldData.chauffeur1_horaire_fin,
                    chauffeur2_horaire_debut: tourData.chauffeur2_horaire_debut ?
                        new Date(tourData.chauffeur2_horaire_debut) : oldData.chauffeur2_horaire_debut,
                    chauffeur2_horaire_fin: tourData.chauffeur2_horaire_fin ?
                        new Date(tourData.chauffeur2_horaire_fin) : oldData.chauffeur2_horaire_fin,
                    RefbonOnyx: tourData.RefbonOnyx,
                };
                return mysql.tour.updateTour(data);
            })
            .then( (dbRes) => {
                if (dbRes.error) {
                    resolve([{ error: true, notFound: true }, 404]);
                }
                if (dbRes.affectedRows == 0) {
                    resolve([{ error: true, message: 'The resource cannot be updated for now.' }, 503]);
                } else {
                    resolve([dbRes, 200]);
                }
            })
            .catch( (err) => reject(err));
    });
}

/**
 * Deletes a tour from the database.
 * @param {Number} tourId The tour Id.
 * @return {Promise} A promise of resolution.
 */
function deleteTour(tourId) {
    return new Promise((resolve, reject) => {
        mysql.tour.deleteTour(tourId)
            .then( (dbRes) => {
                if (dbRes.affectedRows == 0) {
                    resolve([{ error: true, notFound: true }, 404]);
                } else {
                    resolve([dbRes, 200]);
                }
            })
            .catch( (err) => reject(err));
    });
}

export default {
    getAllTours,
    getTourById,
    getCallsForTour,
    createTour,
    updateTour,
    deleteTour,
};
