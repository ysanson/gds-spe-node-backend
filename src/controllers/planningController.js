import mysql from '../database';

/**
 * Gets the prioritary removals from the database.
 * @return {Promise} A promise of resolution.
 */
function getPrioritaryRemovals() {
    return new Promise((resolve, reject) => {
        mysql.planning.getPrioritaryRemovals()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Gets the removals by a specific zone.
 * @param {String} zoneCode The zone code
 * @return {Promise} A promise of resolution.
 */
function getRemovalsByZone(zoneCode) {
    return new Promise((resolve, reject) => {
        const zone = zoneCode.toUpperCase();
        if (zone === 'OUEST' || zone === 'MILIEU' || zone === 'SALAZIE' || zone === 'EST' || zone === 'SUD') {
            mysql.planning.getRemovalsByZone(zone)
                .then( (dbRes) => resolve([dbRes, 200]))
                .catch( (error) => reject(error));
        } else {
            resolve([{ error: true, message: 'Zone code not allowed' }, 400]);
        }
    });
}

export default {
    getPrioritaryRemovals,
    getRemovalsByZone,
};
