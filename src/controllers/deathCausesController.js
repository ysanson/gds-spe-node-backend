import mysql from '../database';

/**
 * Gets the death causes from the database.
 * @return {Promise} A promise with the data.
 */
function getDeathCauses() {
    return new Promise((resolve, reject) => {
        mysql.deathCauses.getCauses()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Gets a death cause by id.
 * @param {Number} cId The cause ID.
 * @return {Promise} A promise with the data.
 */
function getCauseById(cId) {
    return new Promise((resolve, reject) => {
        mysql.deathCauses.getCauseById(cId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

export default {
    getDeathCauses,
    getCauseById,
};
