import 'dotenv/config';
import mysql from '../database';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
/**
 * Checks whether the credentials are valid or not.
 * If yes, a JWT token is issued.
 * @param {JSON} data The data provided (username and password)
 * @return {Promise} A promise of resolution containing the auth state, token, and user.
 */
function loginUser(data) {
    return new Promise((resolve, reject) => {
        let res = {};
        let token;
        let status = 200;
        if ((typeof data.username !== 'undefined')&&(typeof data.password !== 'undefined')) {
            mysql.user.getPasswordFromUser(data.username)
                .then( (dbRes)=>{
                    if (!dbRes.notFound) {
                        const samePwd = bcrypt.compareSync(data.password, dbRes.password);
                        if (samePwd) {
                            token = jwt.sign({ userId: dbRes.id }, process.env.SECRET, { expiresIn: 43200, issuer: 'GDS Reunion' });
                            res = { auth: true, token: token };
                            return mysql.user.getUserById(dbRes.id);
                        } else {
                            res = { auth: false, token: null };
                            status = 401;
                            resolve([res, status]);
                        }
                    } else {
                        res = { auth: false, token: null };
                        status = 404;
                        resolve([res, status]);
                    }
                })
                .then((userData) => {
                    res.user = userData;
                    resolve([res, status]);
                })
                .catch((error) => reject(error));
        } else {
            res = { auth: false, token: null };
            resolve([res, 400]);
        }
    });
}

export default {
    loginUser,
};
