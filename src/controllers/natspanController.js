import mysql from '../database';

/**
 * Gets all the natspan from teh database.
 * @return {Promise} A promise with the data.
 */
function getAllNatspan() {
    return new Promise((resolve, reject) => {
        mysql.natspan.getAllNatspan()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Gets natspan info by ID.
 * @param {Number} nId the natspan id.
 * @return {Promise} A promise with the data.
 */
function getNatspanById(nId) {
    return new Promise((resolve, reject) => {
        mysql.natspan.getNatspanById(nId)
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

export default {
    getAllNatspan,
    getNatspanById,
};
