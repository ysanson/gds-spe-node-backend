import mysql from '../database';

/**
 * Gets all the species from the database.
 * @return {Promise} A promise with the data.
 */
function getAllSpecies() {
    return new Promise((resolve, reject) => {
        mysql.species.getAllSpecies()
            .then( (dbRes) => resolve([dbRes, 200]))
            .catch( (error) => reject(error));
    });
}

/**
 * Gets the species by ID.
 * @param {Number} sId The species ID.
 * @return {Promise} A promise with the data.
 */
function getSpeciesById(sId) {
    return new Promise((resolve, reject) => {
        mysql.species.getSpeciesById(sId)
            .then( (dbRes) => {
                let status = 200;
                if (dbRes.notFound) {
                    status = 404;
                }
                resolve([dbRes, status]);
            })
            .catch( (err) => reject(err));
    });
}

export default {
    getAllSpecies,
    getSpeciesById,
};
