FROM node:10
# Change the timezone to make it Indian/Reunion
ENV TZ=Indian/Reunion
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# RUN npm install
# If you are building your code for production
RUN npm ci --only=production

# Bundle app source
COPY lib/ .
COPY .env .

EXPOSE 3000
CMD [ "node", "index.js" ]